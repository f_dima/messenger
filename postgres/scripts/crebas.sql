/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     17.10.2020 20:24:10                          */
/*==============================================================*/


drop table if exists messages;
drop table if exists groups_users;
drop table if exists groups;
drop table if exists users;
drop table if exists accounts;

/*==============================================================*/
/* Table: accounts                                              */
/*==============================================================*/
create table accounts (
   email                VARCHAR(256)         not null,
   password             VARCHAR(32)          not null,
   is_admin             BOOL                 null,
   first_name           VARCHAR(32)          not null,
   last_name            VARCHAR(32)          null,
   was_deleted          BOOL                 null,
   was_not_confirmed    BOOL                 null,
   was_blocked          BOOL                 null,
   constraint PK_ACCOUNTS primary key (email)
);

/*==============================================================*/
/* Table: users                                                 */
/*==============================================================*/
create table users (
   id                   SERIAL               not null,
   account_id           VARCHAR(256)         not null,
   full_name            VARCHAR(64)          not null,
   photo_url            VARCHAR(64)          null,
   is_available         BOOL                 null,
   constraint PK_USERS primary key (id),
   constraint FK_USERS_REFERENCE_ACCOUNTS foreign key (account_id)
      references accounts (email)
      on delete restrict on update restrict
);

/*==============================================================*/
/* Table: groups                                                */
/*==============================================================*/
create table groups (
   id                   SERIAL               not null,
   name                 VARCHAR(32)          not null,
   owner_id             INT4                 not null,
   is_default_group     BOOL                 null,
   constraint PK_GROUPS primary key (id),
   constraint FK_GROUPS_REFERENCE_USERS foreign key (owner_id)
      references users (id)
      on delete restrict on update restrict
);

/*==============================================================*/
/* Table: groups_users                                          */
/*==============================================================*/
create table groups_users (
   id                   SERIAL               not null,
   group_id             INT4                 not null,
   user_id              INT4                 not null,
   constraint PK_GROUPS_USERS primary key (id),
   constraint AK_KEY_2_GROUPS_U unique (group_id, user_id),
   constraint FK_GROUPS_U_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict,
   constraint FK_GROUPS_U_REFERENCE_GROUPS foreign key (group_id)
      references groups (id)
      on delete restrict on update restrict
);

/*==============================================================*/
/* Table: messages                                              */
/*==============================================================*/
create table messages (
   id                   SERIAL               not null,
   user_from_id         INT4                 not null,
   user_to_id           INT4                 not null,
   content              TEXT                 not null,
   created_date         TIMESTAMP            not null,
   was_deleted          BOOL                 null,
   constraint PK_MESSAGES primary key (id),
   constraint FK_MESSAGES_REFERENCE_USERS foreign key (user_from_id)
      references users (id)
      on delete restrict on update restrict,
   constraint FK_MESSAGES_REFERENCE_USERS2 foreign key (user_to_id)
      references users (id)
      on delete restrict on update restrict
);

