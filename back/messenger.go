package main

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	http_accounts "messenger/back/http/controllers/accounts"
	http_groups "messenger/back/http/controllers/groups"
	http_groupsusers "messenger/back/http/controllers/groupsusers"
	http_messages "messenger/back/http/controllers/messages"
	http_users "messenger/back/http/controllers/users"
	http_wsmsgs "messenger/back/http/controllers/wsmsgs"
	"messenger/back/models/accounts"
	"messenger/back/models/auth"
	"messenger/back/models/groups"
	"messenger/back/models/groupsusers"
	"messenger/back/models/messages"
	"messenger/back/models/users"
	"messenger/back/models/wsmsgs"

	"github.com/go-chi/chi"
	"github.com/go-pg/pg/v10"
	"github.com/rs/cors"
	"go.uber.org/zap"
)

type messenger struct {
	config     *messengerConfig
	pgdb       *pg.DB
	logger     *zap.Logger
	models     *modelSet
	auth       *auth.Storage
	httpServer *http.Server
	wg         *sync.WaitGroup
}

type messengerConfig struct {
	Postgres *pgConfig   `yaml:"postgres"`
	HTTP     *httpConfig `yaml:"http"`
}

type pgConfig struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

type httpConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type modelSet struct {
	User      *users.Model
	Account   *accounts.Model
	GroupUser *groupsusers.Model
	Group     *groups.Model
	Message   *messages.Model
	Wsmsg     *wsmsgs.Model
}

func newMessenger(config *messengerConfig) (*messenger, error) {
	pgConf := config.Postgres

	pgdb := pg.Connect(
		&pg.Options{
			Addr:     fmt.Sprintf("%s:%s", pgConf.Host, pgConf.Port),
			User:     pgConf.User,
			Password: pgConf.Password,
			Database: pgConf.Database,
		},
	)

	err := pgdb.Ping(context.Background())

	if err != nil {
		return nil, err
	}

	logger := zap.NewExample()

	usersModel, err := users.NewModel(
		users.ModelConfig{
			Pgdb: pgdb,
		},
	)

	if err != nil {
		return nil, err
	}

	storage, err := auth.NewStorage()

	if err != nil {
		return nil, err
	}

	accountsModel, err := accounts.NewModel(
		accounts.ModelConfig{
			Pgdb: pgdb,
			Auth: storage,
		},
	)

	if err != nil {
		return nil, err
	}

	groupsusersModel, err := groupsusers.NewModel(
		groupsusers.ModelConfig{
			Pgdb: pgdb,
		},
	)

	if err != nil {
		return nil, err
	}

	groupsModel, err := groups.NewModel(
		groups.ModelConfig{
			Pgdb: pgdb,
		},
	)

	if err != nil {
		return nil, err
	}

	messagesModel, err := messages.NewModel(
		messages.ModelConfig{
			Pgdb: pgdb,
		},
	)

	if err != nil {
		return nil, err
	}

	wsmsgsModel, err := wsmsgs.NewModel(
		wsmsgs.ModelConfig{
			Pgdb:   pgdb,
			Logger: logger,
		},
	)

	if err != nil {
		return nil, err
	}

	m := &messenger{
		pgdb:   pgdb,
		config: config,
		logger: logger,
		models: &modelSet{
			User:      usersModel,
			Account:   accountsModel,
			GroupUser: groupsusersModel,
			Group:     groupsModel,
			Message:   messagesModel,
			Wsmsg:     wsmsgsModel,
		},
		auth: storage,
		wg:   &sync.WaitGroup{},
	}

	err = m.setupHTTPServer(config.HTTP)

	if err != nil {
		return nil, err
	}

	return m, nil
}

func (m *messenger) Run() {
	m.wg.Add(1)

	go func() {
		defer m.wg.Done()

		err := m.httpServer.ListenAndServe()

		if err != http.ErrServerClosed {
			fmt.Println("aa")
			m.logger.Error(err.Error())
		}
	}()
}

func (m *messenger) Stop() {
	err := m.httpServer.Shutdown(context.Background())

	if err != nil {
		m.logger.Error(err.Error())
	}

	m.wg.Wait()
}

func (m *messenger) setupHTTPServer(config *httpConfig) error {
	r := chi.NewRouter()

	r.Group(func(r chi.Router) {
		usersCtr := http_users.NewController(&http_users.Config{
			Users:  m.models.User,
			Auth:   m.auth,
			Logger: m.logger,
		})

		r.Mount("/api/users", usersCtr.NewRouter())

		accountsCtr := http_accounts.NewController(&http_accounts.Config{
			Accounts: m.models.Account,
			Auth:     m.auth,
			Logger:   m.logger,
		})

		r.Mount("/api/accounts", accountsCtr.NewRouter())

		groupsusersCtr := http_groupsusers.NewController(&http_groupsusers.Config{
			Groupsusers: m.models.GroupUser,
			Auth:        m.auth,
			Logger:      m.logger,
		})

		r.Mount("/api/groupsusers", groupsusersCtr.NewRouter())

		groupsCtr := http_groups.NewController(&http_groups.Config{
			Groups: m.models.Group,
			Auth:   m.auth,
			Logger: m.logger,
		})

		r.Mount("/api/groups", groupsCtr.NewRouter())

		messagesCtr := http_messages.NewController(&http_messages.Config{
			Messages: m.models.Message,
			Auth:     m.auth,
			Logger:   m.logger,
		})

		r.Mount("/api/messages", messagesCtr.NewRouter())

		wsmsgsCtr := http_wsmsgs.NewController(&http_wsmsgs.Config{
			Wsmsgs: m.models.Wsmsg,
			Auth:   m.auth,
			Logger: m.logger,
		})

		r.Mount("/api/ws", wsmsgsCtr.NewRouter())
	})

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	handler := c.Handler(r)

	m.httpServer = &http.Server{
		Addr:    fmt.Sprintf("%s:%s", config.Host, config.Port),
		Handler: handler,
	}

	return nil
}
