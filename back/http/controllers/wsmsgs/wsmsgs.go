package wsmsgs

import (
	"messenger/back/models/auth"
	"messenger/back/models/wsmsgs"
	"net/http"

	http_helpers "messenger/back/http/helpers"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.uber.org/zap"
)

// Controller type represents HTTP-controller
type Controller struct {
	wsmsgs *wsmsgs.Model
	auth   *auth.Storage
	logger *zap.Logger
}

// Config type represents config
type Config struct {
	Wsmsgs *wsmsgs.Model
	Auth   *auth.Storage
	Logger *zap.Logger
}

type wsmsgResponse struct {
	*wsmsgs.Wsmsg
}

// NewController method creates new controller instance.
func NewController(config *Config) *Controller {
	return &Controller{
		wsmsgs: config.Wsmsgs,
		auth:   config.Auth,
		logger: config.Logger,
	}
}

// NewRouter method returns HTTP-router for controller.
func (c *Controller) NewRouter() chi.Router {
	r := chi.NewRouter().With(http_helpers.LoggingMiddleware(c.logger)).
		With(http_helpers.AuthorizeMiddleware(c.auth, c.logger))

	r.HandleFunc("/", c.Create)

	return r
}

// Get handler renders wsmsg by email.
func (c *Controller) Get(w http.ResponseWriter, r *http.Request) {

}

// Create handler creates new wsmsg.
func (c *Controller) Create(w http.ResponseWriter, r *http.Request) {
	userContext, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	err = c.wsmsgs.Create(w, r, userContext)

	if err != nil {
		c.logger.Error(err.Error())
		return
	}
}

// Update handler updates wsmsg by email.
func (c *Controller) Update(w http.ResponseWriter, r *http.Request) {

}

// Delete handler deletes wsmsg by email.
func (c *Controller) Delete(w http.ResponseWriter, r *http.Request) {

}

func newWsmsgListResponse(wsmsgs []*wsmsgs.Wsmsg) []render.Renderer {
	list := make([]render.Renderer, len(wsmsgs))

	for i, wsmsg := range wsmsgs {
		list[i] = newWsmsgResponse(wsmsg)
	}

	return list
}

func newWsmsgResponse(wsmsg *wsmsgs.Wsmsg) *wsmsgResponse {
	return &wsmsgResponse{
		Wsmsg: wsmsg,
	}
}

func (u *wsmsgResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
