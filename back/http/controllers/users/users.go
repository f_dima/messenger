package users

import (
	"fmt"
	"io"
	"messenger/back/models/auth"
	"messenger/back/models/users"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"

	http_helpers "messenger/back/http/helpers"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

const tempDir = "/tmp/messenger"

// Controller type represents HTTP-controller
type Controller struct {
	users  *users.Model
	auth   *auth.Storage
	logger *zap.Logger
}

// Config type represents config
type Config struct {
	Users  *users.Model
	Auth   *auth.Storage
	Logger *zap.Logger
}

type userResponse struct {
	*users.User
}

type listForm struct {
	Filters map[string]interface{}
}

// NewController method creates new controller instance.
func NewController(config *Config) *Controller {
	return &Controller{
		users:  config.Users,
		auth:   config.Auth,
		logger: config.Logger,
	}
}

// NewRouter method returns HTTP-router for controller.
func (c *Controller) NewRouter() chi.Router {
	r := chi.NewRouter().With(http_helpers.LoggingMiddleware(c.logger)).
		With(http_helpers.AuthorizeMiddleware(c.auth, c.logger))

	r.With(http_helpers.Paginate).Get("/", c.List)
	r.Get("/files/{directory}/{fileName}", c.GetPhoto)
	r.Post("/files", c.UploadPhoto)
	r.With(http_helpers.Paginate).Get("/search", c.Search)
	r.With(http_helpers.Paginate).Get("/search_in_group", c.SearchInGroup)
	r.Post("/", c.Create)

	r.Route("/{userID}",
		func(r chi.Router) {
			r.Get("/", c.Get)
			r.Put("/", c.Update)
			r.Delete("/", c.Delete)
		},
	)

	return r
}

// List handler renders users list.
func (c *Controller) List(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	groupIDStr := r.FormValue("group_id")
	var groupID *int

	if groupIDStr != "" {
		id, err := strconv.Atoi(groupIDStr)

		if err != nil {
			c.logger.Error(err.Error())
			http_helpers.InternalServerError(w, r)
			return
		}

		groupID = &id
	}

	isDefGroupStr := r.FormValue("is_default_group")
	var isDefGroup bool

	if isDefGroupStr == "true" {
		isDefGroup = true
	}

	ctx := r.Context()
	pgn := ctx.Value(http_helpers.PaginatorContextKey).(*http_helpers.Paginator)

	users, total, err := c.users.List(userID, groupID, isDefGroup, pgn.Skip(), pgn.Limit())

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	pgn.Total = total
	pgn.SetHeaders(w, r)
	render.RenderList(w, r, newUserListResponse(users))
}

// Search handler searches users.
func (c *Controller) Search(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	query := r.FormValue("query")

	if query == "" {
		c.logger.Error("query not specified")
		http_helpers.InternalServerError(w, r)
		return
	}

	query, err = url.PathUnescape(query)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	ctx := r.Context()
	pgn := ctx.Value(http_helpers.PaginatorContextKey).(*http_helpers.Paginator)

	users, total, err := c.users.Search(userID, query, pgn.Skip(), pgn.Limit())

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	pgn.Total = total
	pgn.SetHeaders(w, r)
	render.RenderList(w, r, newUserListResponse(users))
}

// SearchInGroup handler searches users in group.
func (c *Controller) SearchInGroup(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	query := r.FormValue("query")

	if query == "" {
		c.logger.Error("query not specified")
		http_helpers.InternalServerError(w, r)
		return
	}

	query, err = url.PathUnescape(query)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	groupIDStr := r.FormValue("group_id")
	var groupID int

	if groupIDStr == "" {
		c.logger.Error("group_id not specified")
		http_helpers.InternalServerError(w, r)
		return
	}

	groupID, err = strconv.Atoi(groupIDStr)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	ctx := r.Context()
	pgn := ctx.Value(http_helpers.PaginatorContextKey).(*http_helpers.Paginator)

	users, total, err := c.users.SearchInGroup(userID, groupID, query,
		pgn.Skip(), pgn.Limit())

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	pgn.Total = total
	pgn.SetHeaders(w, r)
	render.RenderList(w, r, newUserListResponse(users))
}

// Get handler renders user by email.
func (c *Controller) Get(w http.ResponseWriter, r *http.Request) {

}

// GetPhoto handler renders user by email.
func (c *Controller) GetPhoto(w http.ResponseWriter, r *http.Request) {
	dirName := chi.URLParam(r, "directory")

	if dirName == "" {
		http_helpers.NotFound(w, r, fmt.Errorf("photo not found"))
		return
	}

	fileName := chi.URLParam(r, "fileName")

	if fileName == "" {
		http_helpers.NotFound(w, r, fmt.Errorf("photo not found"))
		return
	}

	file, err := os.Open(filepath.Join(tempDir, dirName, fileName))

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	defer file.Close()

	_, err = io.Copy(w, file)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// UploadPhoto handler renders user by email.
func (c *Controller) UploadPhoto(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	err = r.ParseMultipartForm(10 * 1024 * 1025)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	photo, _, err := r.FormFile("user-photo")

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	defer photo.Close()

	userDir := fmt.Sprintf("user%d", userID)
	dirName := filepath.Join(tempDir, userDir)
	fileName := fmt.Sprintf("%s.jpg", uuid.New().String())
	filePath := filepath.Join(dirName, fileName)

	err = os.RemoveAll(dirName)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	err = os.MkdirAll(dirName, 0777)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	newFile, err := os.Create(filePath)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	defer newFile.Close()

	_, err = io.Copy(newFile, photo)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	photoURL := fmt.Sprintf("/api/users/files/%s/%s", userDir, fileName)

	w.Write([]byte(photoURL))
	w.WriteHeader(http.StatusCreated)
}

// Create handler creates new user.
func (c *Controller) Create(w http.ResponseWriter, r *http.Request) {

}

// Update handler updates user by email.
func (c *Controller) Update(w http.ResponseWriter, r *http.Request) {

}

// Delete handler deletes user by email.
func (c *Controller) Delete(w http.ResponseWriter, r *http.Request) {

}

func newUserListResponse(users []*users.User) []render.Renderer {
	list := make([]render.Renderer, len(users))

	for i, user := range users {
		list[i] = newUserResponse(user)
	}

	return list
}

func newUserResponse(user *users.User) *userResponse {
	return &userResponse{
		User: user,
	}
}

func (u *userResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
