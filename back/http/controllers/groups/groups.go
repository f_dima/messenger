package groups

import (
	"errors"
	"messenger/back/models/auth"
	"messenger/back/models/groups"
	"net/http"
	"strconv"

	http_helpers "messenger/back/http/helpers"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.uber.org/zap"
)

// Controller type represents HTTP-controller
type Controller struct {
	groups *groups.Model
	auth   *auth.Storage
	logger *zap.Logger
}

// Config type represents config
type Config struct {
	Groups *groups.Model
	Auth   *auth.Storage
	Logger *zap.Logger
}

type groupResponse struct {
	*groups.Group
}

type groupRequest struct {
	*groups.Group
}

type listForm struct {
	Filters map[string]interface{}
}

// NewController method creates new controller instance.
func NewController(config *Config) *Controller {
	return &Controller{
		groups: config.Groups,
		auth:   config.Auth,
		logger: config.Logger,
	}
}

// NewRouter method returns HTTP-router for controller.
func (c *Controller) NewRouter() chi.Router {
	r := chi.NewRouter().With(http_helpers.LoggingMiddleware(c.logger)).
		With(http_helpers.AuthorizeMiddleware(c.auth, c.logger))

	r.Get("/", c.List)
	r.Post("/", c.Create)

	r.Route("/{groupID}",
		func(r chi.Router) {
			r.Get("/", c.Get)
			r.Put("/", c.Update)
			r.Delete("/", c.Delete)
		},
	)

	return r
}

// List handler renders groups list.
func (c *Controller) List(w http.ResponseWriter, r *http.Request) {
	userContext, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	userIDStr := r.FormValue("user_id")
	var userID *int

	if userIDStr != "" {
		id, err := strconv.Atoi(userIDStr)

		if err != nil {
			c.logger.Error(err.Error())
			http_helpers.InternalServerError(w, r)
			return
		}

		userID = &id
	}

	groups, err := c.groups.List(userContext, userID)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	render.RenderList(w, r, newGroupListResponse(groups))
}

// Get handler renders user by email.
func (c *Controller) Get(w http.ResponseWriter, r *http.Request) {

}

// Create handler creates new user.
func (c *Controller) Create(w http.ResponseWriter, r *http.Request) {
	payload := &groupRequest{}
	err := render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newGroup := payload.Group
	errs := http_helpers.ValidateStruct(newGroup, nil)

	if errs != nil {
		http_helpers.ValidationFailed(w, r, errs)
		return
	}

	userContext, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	err = c.groups.Create(userContext, newGroup)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

// Update handler updates group by email.
func (c *Controller) Update(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "groupID")

	if idStr == "" {
		http_helpers.NotFound(w, r, groups.ErrNotFound)
		return
	}

	id, err := strconv.Atoi(idStr)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	payload := &groupRequest{}
	err = render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newGroup := payload.Group

	errs := http_helpers.ValidateStruct(newGroup, nil)

	if errs != nil {
		http_helpers.ValidationFailed(w, r, errs)
		return
	}

	if id != newGroup.ID {
		http_helpers.Conflict(w, r, err)
		return
	}

	err = c.groups.Update(newGroup)

	if err != nil {
		if err == groups.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Delete handler deletes group by id.
func (c *Controller) Delete(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "groupID")

	if idStr == "" {
		http_helpers.NotFound(w, r, groups.ErrNotFound)
		return
	}

	id, err := strconv.Atoi(idStr)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	err = c.groups.Delete(id)

	if err != nil {
		if err == groups.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func newGroupListResponse(groups []*groups.Group) []render.Renderer {
	list := make([]render.Renderer, len(groups))

	for i, group := range groups {
		list[i] = newGroupResponse(group)
	}

	return list
}

func newGroupResponse(group *groups.Group) *groupResponse {
	return &groupResponse{
		Group: group,
	}
}

func (g *groupResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (g *groupRequest) Bind(r *http.Request) error {
	if g.Group == nil {
		return errors.New("missing required Group field")
	}

	return nil
}
