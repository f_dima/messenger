package groupsusers

import (
	"errors"
	"messenger/back/models/auth"
	"messenger/back/models/groupsusers"
	"messenger/back/models/users"
	"net/http"

	http_helpers "messenger/back/http/helpers"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.uber.org/zap"
)

// Controller type represents HTTP-controller
type Controller struct {
	groupsusers *groupsusers.Model
	auth        *auth.Storage
	logger      *zap.Logger
}

// Config type represents config
type Config struct {
	Groupsusers *groupsusers.Model
	Auth        *auth.Storage
	Logger      *zap.Logger
}

type userResponse struct {
	*users.User
}

type groupuserRequest struct {
	*groupsusers.GroupUser
}

type listForm struct {
	Filters map[string]interface{}
}

// NewController method creates new controller instance.
func NewController(config *Config) *Controller {
	return &Controller{
		groupsusers: config.Groupsusers,
		auth:        config.Auth,
		logger:      config.Logger,
	}
}

// NewRouter method returns HTTP-router for controller.
func (c *Controller) NewRouter() chi.Router {
	r := chi.NewRouter().With(http_helpers.LoggingMiddleware(c.logger)).
		With(http_helpers.AuthorizeMiddleware(c.auth, c.logger))

	r.Post("/", c.Add)
	r.Delete("/", c.Delete)

	return r
}

// Add handler adds user to group.
func (c *Controller) Add(w http.ResponseWriter, r *http.Request) {
	userContext, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	payload := &groupuserRequest{}
	err = render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newGroupUser := payload.GroupUser
	errs := http_helpers.ValidateStruct(newGroupUser, nil)

	if errs != nil {
		http_helpers.ValidationFailed(w, r, errs)
		return
	}

	err = c.groupsusers.Add(userContext, newGroupUser)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

// Delete handler deletes user from group.
func (c *Controller) Delete(w http.ResponseWriter, r *http.Request) {
	userContext, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	payload := &groupuserRequest{}
	err = render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newGroupUser := payload.GroupUser
	errs := http_helpers.ValidateStruct(newGroupUser, nil)

	if errs != nil {
		http_helpers.ValidationFailed(w, r, errs)
		return
	}

	err = c.groupsusers.Delete(userContext, newGroupUser)

	if err != nil {
		if err == groupsusers.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func newUserListResponse(users []*users.User) []render.Renderer {
	list := make([]render.Renderer, len(users))

	for i, user := range users {
		list[i] = newUserResponse(user)
	}

	return list
}

func newUserResponse(user *users.User) *userResponse {
	return &userResponse{
		User: user,
	}
}

func (u *userResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (g *groupuserRequest) Bind(r *http.Request) error {
	if g.GroupUser == nil {
		return errors.New("missing required GroupUser field")
	}

	return nil
}
