package messages

import (
	"messenger/back/models/auth"
	"messenger/back/models/messages"
	"net/http"
	"strconv"

	http_helpers "messenger/back/http/helpers"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.uber.org/zap"
)

// Controller type represents HTTP-controller
type Controller struct {
	messages *messages.Model
	auth     *auth.Storage
	logger   *zap.Logger
}

// Config type represents config
type Config struct {
	Messages *messages.Model
	Auth     *auth.Storage
	Logger   *zap.Logger
}

type messageResponse struct {
	*messages.Message
}

type listForm struct {
	Filters map[string]interface{}
}

// NewController method creates new controller instance.
func NewController(config *Config) *Controller {
	return &Controller{
		messages: config.Messages,
		auth:     config.Auth,
		logger:   config.Logger,
	}
}

// NewRouter method returns HTTP-router for controller.
func (c *Controller) NewRouter() chi.Router {
	r := chi.NewRouter().With(http_helpers.LoggingMiddleware(c.logger)).
		With(http_helpers.AuthorizeMiddleware(c.auth, c.logger))

	r.With(http_helpers.Paginate).Get("/", c.List)

	r.Route("/{messageID}",
		func(r chi.Router) {
			r.Get("/", c.Get)
			r.Put("/", c.Update)
			r.Delete("/", c.Delete)
		},
	)

	return r
}

// List handler renders messages list.
func (c *Controller) List(w http.ResponseWriter, r *http.Request) {
	userContextID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	userIDStr := r.FormValue("user_id")

	if userIDStr == "" {
		c.logger.Error("user_id not specified")
		http_helpers.InternalServerError(w, r)
		return
	}

	userID, err := strconv.Atoi(userIDStr)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	ctx := r.Context()
	pgn := ctx.Value(http_helpers.PaginatorContextKey).(*http_helpers.Paginator)

	messages, total, err := c.messages.List(userContextID,
		userID, pgn.Skip(), pgn.Limit())

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	pgn.Total = total
	pgn.SetHeaders(w, r)
	render.RenderList(w, r, newMessageListResponse(messages))
}

// Get handler renders message by email.
func (c *Controller) Get(w http.ResponseWriter, r *http.Request) {

}

// Create handler creates new message.
func (c *Controller) Create(w http.ResponseWriter, r *http.Request) {

}

// Update handler updates message by email.
func (c *Controller) Update(w http.ResponseWriter, r *http.Request) {

}

// Delete handler deletes message by email.
func (c *Controller) Delete(w http.ResponseWriter, r *http.Request) {

}

func newMessageListResponse(messages []*messages.Message) []render.Renderer {
	list := make([]render.Renderer, len(messages))

	for i, message := range messages {
		list[i] = newMessageResponse(message)
	}

	return list
}

func newMessageResponse(message *messages.Message) *messageResponse {
	return &messageResponse{
		Message: message,
	}
}

func (u *messageResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
