package accounts

import (
	"crypto/md5"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"messenger/back/models/accounts"
	"messenger/back/models/auth"
	"messenger/back/models/users"
	"net/http"
	"net/url"
	"strconv"

	http_helpers "messenger/back/http/helpers"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.uber.org/zap"
)

const (
	robokassaLogin = "messenger-mephi"
	robokassaPass1 = "mephi1234"
	robokassaPass2 = "mephi12345"
)

var errorContent = []byte(`<!DOCTYPE html>
<html>
<body>

<p>Платеж не совершен.</p>
<a href="http://localhost/settings">Назад</a>

</body>
</html>`)

// Controller type represents HTTP-controller
type Controller struct {
	accounts *accounts.Model
	auth     *auth.Storage
	logger   *zap.Logger
}

// Config type represents config
type Config struct {
	Accounts *accounts.Model
	Auth     *auth.Storage
	Logger   *zap.Logger
}

type accountRequest struct {
	*accounts.Account
}

type accountResponse struct {
	*accounts.Account
}

type userResponse struct {
	*users.User
}

type listForm struct {
	Filters map[string]interface{}
}

type operationStateResponse struct {
	Result  *operationResult `xml:"Result"`
	XMLName struct{}         `xml:"OperationStateResponse"`
}

type operationResult struct {
	Сode        int    `xml:"Сode"`
	Description string `xml:"Description"`
}

// NewController method creates new controller instance.
func NewController(config *Config) *Controller {
	return &Controller{
		accounts: config.Accounts,
		auth:     config.Auth,
		logger:   config.Logger,
	}
}

// NewRouter method returns HTTP-router for controller.
func (c *Controller) NewRouter() chi.Router {
	r := chi.NewRouter().With(http_helpers.LoggingMiddleware(c.logger))

	r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
		Get("/", c.List)
	r.Post("/", c.Create)
	r.Get("/confirm", c.Confirm)
	r.Post("/login", c.Login)
	r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
		Get("/logout", c.Logout)
	r.Get("/donate/success", c.DonateSuccess)
	r.Get("/donate/fail", c.DonateFail)

	r.Route("/{userID}",
		func(r chi.Router) {
			r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
				Get("/", c.Get)
		},
	)

	r.Route("/current_user",
		func(r chi.Router) {
			r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
				Get("/", c.GetCurrentUser)
			r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
				Put("/", c.UpdateCurrentUser)
			r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
				Put("/password", c.UpdatePassword)
			r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).
				Delete("/", c.DeleteCurrentUser)
		},
	)

	r.With(http_helpers.AuthorizeMiddleware(c.auth, c.logger)).Route("/admin",
		func(r chi.Router) {
			r.With(http_helpers.Paginate).Get("/search", c.AdminSearch)
			r.Put("/block", c.Block)
			r.Put("/unblock", c.Unblock)
			r.Put("/set_admin", c.SetAdmin)
		},
	)

	return r
}

// List handler renders accounts list.
func (c *Controller) List(w http.ResponseWriter, r *http.Request) {
	accounts, err := c.accounts.List()

	if err != nil {
		http_helpers.InternalServerError(w, r)
		return
	}

	render.RenderList(w, r, newAccountListResponse(accounts))
}

// DonateSuccess handler renders accounts list.
func (c *Controller) DonateSuccess(w http.ResponseWriter, r *http.Request) {
	invoiceID := r.FormValue("InvId")

	if invoiceID == "" {
		http_helpers.BadRequest(w, r, fmt.Errorf("InvId not specified"))
		return
	}

	sum := r.FormValue("OutSum")

	if sum == "" {
		http_helpers.BadRequest(w, r, fmt.Errorf("OutSum not specified"))
		return
	}

	inSignature := r.FormValue("SignatureValue")

	if inSignature == "" {
		http_helpers.BadRequest(w, r, fmt.Errorf("SignatureValue not specified"))
		return
	}

	signForCheck := md5.Sum([]byte(fmt.Sprintf("%s:%s:%s",
		sum, invoiceID, robokassaPass1)))

	if inSignature != fmt.Sprintf("%x", signForCheck) {
		c.logger.Warn("unexpected signature")
		w.Write(errorContent)
		return
	}

	signature := md5.Sum([]byte(fmt.Sprintf("%s:%s:%s",
		robokassaLogin, invoiceID, robokassaPass2)))

	url := fmt.Sprintf("https://auth.robokassa.ru/Merchant/WebService/Service.asmx/OpState?MerchantLogin=%s&InvoiceID=%s&Signature=%x",
		robokassaLogin, invoiceID, signature)

	resp, err := http.Get(url)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		errMsg := fmt.Sprintf("unexpected status %s", resp.Status)
		c.logger.Error(errMsg)
		http_helpers.InternalServerError(w, r)
		return
	}

	data, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	respXML := &operationStateResponse{}
	err = xml.Unmarshal(data, respXML)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	if respXML.Result.Сode != 0 {
		errMsg := fmt.Sprintf("unexpected operation status %d %s",
			respXML.Result.Сode, respXML.Result.Description)
		c.logger.Warn(errMsg)
		w.Write(errorContent)
		return
	}

	w.Write([]byte(fmt.Sprintf(`<!DOCTYPE html>
	<html>
	<body>
	
	<p>Платеж на сумму %s успешно выполнен.</p>
	<a href="http://localhost/settings">Назад</a>
	
	</body>
	</html>`, sum)))
}

// DonateFail handler renders accounts list.
func (c *Controller) DonateFail(w http.ResponseWriter, r *http.Request) {
	w.Write(errorContent)
}

// GetCurrentUser handler renders account by email.
func (c *Controller) GetCurrentUser(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	acc, err := c.accounts.GetCurrentUser(userID)

	if err != nil {
		if err == accounts.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	render.Render(w, r, newAccountResponse(acc))
}

// Get handler renders account by email.
func (c *Controller) Get(w http.ResponseWriter, r *http.Request) {
	userIDStr := chi.URLParam(r, "userID")

	if userIDStr == "" {
		http_helpers.NotFound(w, r, accounts.ErrNotFound)
		return
	}

	userID, err := strconv.Atoi(userIDStr)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	acc, err := c.accounts.Get(userID)

	if err != nil {
		if err == accounts.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	render.Render(w, r, newAccountResponse(acc))
}

// Create handler creates new account.
func (c *Controller) Create(w http.ResponseWriter, r *http.Request) {
	payload := &accountRequest{}
	err := render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newAccount := payload.Account
	errs := http_helpers.ValidateStruct(newAccount, nil)

	if errs != nil {
		http_helpers.ValidationFailed(w, r, errs)
		return
	}

	err = c.accounts.Create(newAccount)

	if err != nil {
		if err == accounts.ErrExists {
			http_helpers.Conflict(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	claims := map[string]interface{}{
		"email": newAccount.Email,
	}

	token, err := accounts.CreateToken(claims)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	// err = accounts.SendEmail(newAccount.Email, token)

	// if err != nil {
	// 	c.logger.Warn(err.Error())
	// }

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(token))
}

// UpdateCurrentUser handler updates account by email.
func (c *Controller) UpdateCurrentUser(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	payload := &accountRequest{}
	err = render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newAccount := payload.Account

	errs := http_helpers.ValidateStruct(newAccount, nil)

	if errs != nil {
		http_helpers.ValidationFailed(w, r, errs)
		return
	}

	err = c.accounts.UpdateCurrentUser(userID, newAccount)

	if err != nil {
		if err == accounts.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		if err == accounts.ErrConflict {
			http_helpers.Conflict(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// UpdatePassword handler updates account by email.
func (c *Controller) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	payload := &accountRequest{}
	err = render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	newAccount := payload.Account
	err = c.accounts.UpdatePassword(userID, newAccount)

	if err != nil {
		if err == accounts.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		if err == accounts.ErrConflict {
			http_helpers.Conflict(w, r, err)
			return
		}

		if err == accounts.ErrInvalidPass {
			http_helpers.BadRequest(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// DeleteCurrentUser handler deletes account by email.
func (c *Controller) DeleteCurrentUser(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	err = c.accounts.DeleteCurrentUser(userID)

	if err != nil {
		if err == accounts.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Confirm handler confirms account by token.
func (c *Controller) Confirm(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("token")

	if token == "" {
		http_helpers.NotFound(w, r, accounts.ErrNotFound)
		return
	}

	token, err := url.PathUnescape(token)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	claims, err := accounts.GetClaimsFromToken(token)

	if err != nil {
		http_helpers.NotFound(w, r, accounts.ErrNotFound)
		return
	}

	email, ok := claims["email"].(string)

	if !ok {
		http_helpers.NotFound(w, r, accounts.ErrNotFound)
		return
	}

	err = c.accounts.Confirm(email)

	if err != nil {
		if err == accounts.ErrNotFound {
			http_helpers.NotFound(w, r, err)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	//http.Redirect(w, r, "/login", http.StatusPermanentRedirect)
}

// Login handler logins user.
func (c *Controller) Login(w http.ResponseWriter, r *http.Request) {
	payload := &accountRequest{}
	err := render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	loginData := &accounts.LoginData{
		Email:    payload.Account.Email,
		Password: payload.Account.Password,
	}

	if loginData.Email == "" || loginData.Password == "" {
		http_helpers.BadRequest(w, r, fmt.Errorf("email or password not specified"))
		return
	}

	cookie, err := c.accounts.Login(loginData)

	if err != nil {
		if err == accounts.ErrUnauthorized {
			http_helpers.Unauthorized(w, r)
			return
		}

		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	http.SetCookie(w, cookie)
	w.WriteHeader(http.StatusOK)
}

// Logout handler logouts user.
func (c *Controller) Logout(w http.ResponseWriter, r *http.Request) {
	deletedCookie := &http.Cookie{
		Name:     "messenger_session",
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}

	cookie, err := r.Cookie("messenger_session")

	if err != nil {
		http.SetCookie(w, deletedCookie)
		w.WriteHeader(http.StatusOK)
		return
	}

	session := cookie.Value

	err = c.accounts.Logout(session)

	if err != nil {
		http_helpers.InternalServerError(w, r)
		return
	}

	http.SetCookie(w, deletedCookie)
	w.WriteHeader(http.StatusOK)
}

// Block handler logouts user.
func (c *Controller) Block(w http.ResponseWriter, r *http.Request) {
	payload := &accountRequest{}
	err := render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	err = c.accounts.Block(payload.Account.Email)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Unblock handler logouts user.
func (c *Controller) Unblock(w http.ResponseWriter, r *http.Request) {
	payload := &accountRequest{}
	err := render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	err = c.accounts.Unblock(payload.Account.Email)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// SetAdmin handler logouts user.
func (c *Controller) SetAdmin(w http.ResponseWriter, r *http.Request) {
	payload := &accountRequest{}
	err := render.Bind(r, payload)

	if err != nil {
		http_helpers.BadRequest(w, r, err)
		return
	}

	err = c.accounts.SetAdmin(payload.Account.Email)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// AdminSearch handler searches users.
func (c *Controller) AdminSearch(w http.ResponseWriter, r *http.Request) {
	userID, err := http_helpers.GetUserID(r)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	query := r.FormValue("query")

	if query == "" {
		c.logger.Error("query not specified")
		http_helpers.InternalServerError(w, r)
		return
	}

	query, err = url.PathUnescape(query)

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	blockFilter := r.FormValue("block_filter")
	ctx := r.Context()
	pgn := ctx.Value(http_helpers.PaginatorContextKey).(*http_helpers.Paginator)

	users, total, err := c.accounts.AdminSearch(userID, query,
		blockFilter, pgn.Skip(), pgn.Limit())

	if err != nil {
		c.logger.Error(err.Error())
		http_helpers.InternalServerError(w, r)
		return
	}

	pgn.Total = total
	pgn.SetHeaders(w, r)
	render.RenderList(w, r, newUserListResponse(users))
}

func newAccountListResponse(accounts []*accounts.Account) []render.Renderer {
	list := make([]render.Renderer, len(accounts))

	for i, account := range accounts {
		list[i] = newAccountResponse(account)
	}

	return list
}

func newAccountResponse(account *accounts.Account) *accountResponse {
	return &accountResponse{
		Account: account,
	}
}

func (acc *accountRequest) Bind(r *http.Request) error {
	if acc.Account == nil {
		return errors.New("missing required Account field")
	}

	return nil
}

func (a *accountResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func newUserListResponse(users []*users.User) []render.Renderer {
	list := make([]render.Renderer, len(users))

	for i, user := range users {
		list[i] = newUserResponse(user)
	}

	return list
}

func newUserResponse(user *users.User) *userResponse {
	return &userResponse{
		User: user,
	}
}

func (u *userResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
