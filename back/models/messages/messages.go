package messages

import (
	"errors"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

var (
	// ErrExists message exists.
	ErrExists = errors.New("message exists")

	// ErrNotFound message not found.
	ErrNotFound = errors.New("message not found")
)

// Model type reperesents message model.
type Model struct {
	pgdb *pg.DB
}

// ModelConfig type reperesents message model config.
type ModelConfig struct {
	Pgdb *pg.DB
}

// Message type represents message
type Message struct {
	ID          int        `json:"id" pg:"id,pk"`
	UserFromID  int        `json:"user_from_id" pg:"user_from_id" validate:"required"`
	UserToID    int        `json:"user_to_id" pg:"user_to_id" validate:"required"`
	Content     string     `json:"content" pg:"content" validate:"required"`
	CreatedDate *time.Time `json:"created_date" pg:"created_date"`
	WasDeleted  bool       `json:"-" pg:"was_deleted,use_zero"`
	tableName   struct{}   `pg:"messages"`
}

// NewModel method creates new model instance.
func NewModel(config ModelConfig) (*Model, error) {
	m := &Model{
		pgdb: config.Pgdb,
	}

	return m, nil
}

// List method returns messages list.
func (m *Model) List(userContext int, userID int,
	skip, limit int) ([]*Message, int, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, 0, err
	}

	var messages []*Message
	total, err := tx.Model(&messages).Where("was_deleted = ?", false).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			q = q.WhereOr("user_from_id = ? and user_to_id = ?", userContext, userID).
				WhereOr("user_to_id = ? and user_from_id = ?", userContext, userID)
			return q, nil
		}).Order("created_date desc").
		Offset(skip).Limit(limit).SelectAndCount()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	return messages, total, tx.Commit()
}

// Get method gets message by email.
func (m *Model) Get(email string) (*Message, error) {

	return nil, nil
}

// Create method creates new message.
func (m *Model) Create(message *Message) error {

	return nil
}

// Update method updates message by email.
func (m *Model) Update(email string, message *Message) error {

	return nil
}

// Delete method deletes message by email.
func (m *Model) Delete(email string) error {

	return nil
}

// GetDB gets postgres db
func (m *Model) GetDB() *pg.DB {
	return m.pgdb
}
