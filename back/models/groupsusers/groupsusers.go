package groupsusers

import (
	"errors"
	"messenger/back/models/groups"

	"github.com/go-pg/pg/v10"
)

var (
	// ErrExists group exists.
	ErrExists = errors.New("user in this group adlready exists")

	// ErrNotFound user in this group not found.
	ErrNotFound = errors.New("user in this group not found")
)

// Model type reperesents group model.
type Model struct {
	pgdb *pg.DB
}

// ModelConfig type reperesents group model config.
type ModelConfig struct {
	Pgdb *pg.DB
}

// GroupUser type represents group
type GroupUser struct {
	ID             int      `json:"-" pg:"id,pk"`
	GroupID        int      `json:"group_id" pg:"group_id" validate:"required"`
	UserID         int      `json:"user_id" pg:"user_id" validate:"required"`
	IsDefaultGroup bool     `json:"is_default_group,omitempty" pg:"-"`
	tableName      struct{} `pg:"groups_users"`
}

// NewModel method creates new model instance.
func NewModel(config ModelConfig) (*Model, error) {
	m := &Model{
		pgdb: config.Pgdb,
	}

	return m, nil
}

// Add method adds user to group.
func (m *Model) Add(userContext int, groupuser *GroupUser) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	if groupuser.IsDefaultGroup {
		group := &groups.Group{
			OwnerID:        userContext,
			IsDefaultGroup: groupuser.IsDefaultGroup,
		}

		err = tx.Model(group).Where("owner_id = ?", group.OwnerID).
			Where("is_default_group = ?", group.IsDefaultGroup).Select()

		if err != nil {
			tx.Rollback()
			return err
		}

		groupuser.GroupID = group.ID
	}

	_, err = tx.Model(groupuser).Insert()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// Delete method deletes user from group.
func (m *Model) Delete(userContext int, groupuser *GroupUser) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	isExists, err := tx.Model(groupuser).Where("group_id = ?", groupuser.GroupID).
		Where("user_id = ?", groupuser.UserID).Exists()

	if err != nil {
		tx.Rollback()
		return err
	}

	if !isExists {
		tx.Rollback()
		return ErrNotFound
	}

	groupIDs := []int{groupuser.GroupID}

	if groupuser.IsDefaultGroup {
		var groups []*groups.Group

		err = tx.Model(&groups).Where("owner_id = ?", userContext).
			Where("is_default_group = ?", false).Select()

		if err != nil {
			tx.Rollback()
			return err
		}

		for _, v := range groups {
			groupIDs = append(groupIDs, v.ID)
		}
	}

	_, err = tx.Model(groupuser).Where("group_id in (?)", pg.In(groupIDs)).
		Where("user_id = ?", groupuser.UserID).Delete()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}
