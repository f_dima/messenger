package users

import (
	"errors"
	"fmt"
	"messenger/back/models/groups"
	"messenger/back/models/groupsusers"
	"messenger/back/models/messages"
	"sort"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

var (
	// ErrExists user exists.
	ErrExists = errors.New("user exists")

	// ErrNotFound user not found.
	ErrNotFound = errors.New("user not found")
)

// Model type reperesents user model.
type Model struct {
	pgdb *pg.DB
}

// ModelConfig type reperesents user model config.
type ModelConfig struct {
	Pgdb *pg.DB
}

// User type represents user
type User struct {
	ID          int               `json:"id" pg:"id,pk"`
	AccountID   string            `json:"account_id" pg:"account_id" validate:"required"`
	FullName    string            `json:"full_name" pg:"full_name" validate:"required"`
	PhotoURL    string            `json:"photo_url,omitempty" pg:"photo_url"`
	IsAvailable bool              `json:"-" pg:"is_available,use_zero"`
	IsAdmin     bool              `json:"is_admin,omitempty" pg:"-"`
	WasBlocked  bool              `json:"was_blocked,omitempty" pg:"-"`
	LastMessage *messages.Message `json:"last_message,omitempty" pg:"-"`
	tableName   struct{}          `pg:"users"`
}

// NewModel method creates new model instance.
func NewModel(config ModelConfig) (*Model, error) {
	m := &Model{
		pgdb: config.Pgdb,
	}

	return m, nil
}

// List method returns users list.
func (m *Model) List(userContext int, groupID *int, isDefaultGroup bool,
	skip, limit int) ([]*User, int, error) {
	if groupID == nil {
		var users []*User
		total, err := m.pgdb.Model(&users).Where("is_available = ?", true).
			Where("id != ?", userContext).
			Order("full_name asc").
			Offset(skip).Limit(limit).SelectAndCount()

		if err != nil && err != pg.ErrNoRows {
			return nil, 0, err
		}

		return users, total, nil
	}

	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, 0, err
	}

	if isDefaultGroup {
		group := &groups.Group{
			OwnerID:        userContext,
			IsDefaultGroup: isDefaultGroup,
		}

		err = tx.Model(group).Where("owner_id = ?", group.OwnerID).
			Where("is_default_group = ?", group.IsDefaultGroup).Select()

		if err != nil {
			tx.Rollback()
			return nil, 0, err
		}

		groupID = &group.ID
	}

	var users []*User
	var groupusers []*groupsusers.GroupUser
	err = tx.Model(&groupusers).Where("group_id = ?", groupID).Select()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	if len(groupusers) == 0 {
		return users, 0, nil
	}

	userIDs := make([]int, len(groupusers))

	for i, v := range groupusers {
		userIDs[i] = v.UserID
	}

	total, err := tx.Model(&users).Where("id in (?)", pg.In(userIDs)).
		Where("is_available = ?", true).
		Order("full_name asc").SelectAndCount()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	for _, user := range users {
		msg := &messages.Message{
			WasDeleted: false,
		}

		err := tx.Model(msg).
			Where("was_deleted = ?", msg.WasDeleted).
			WhereGroup(func(q *orm.Query) (*orm.Query, error) {
				q = q.WhereOr("user_from_id = ? and user_to_id = ?", userContext, user.ID).
					WhereOr("user_to_id = ? and user_from_id = ?", userContext, user.ID)
				return q, nil
			}).Order("created_date desc").Limit(1).Select()

		if err != nil && err != pg.ErrNoRows {
			tx.Rollback()
			return nil, 0, err
		}

		if err == pg.ErrNoRows {
			msg = nil
		}

		user.LastMessage = msg
	}

	sort.SliceStable(users, func(i, j int) bool {
		msg1 := users[i].LastMessage
		msg2 := users[j].LastMessage

		if msg1 != nil && msg2 == nil ||
			msg1 != nil && msg2 != nil && msg1.CreatedDate.After(*msg2.CreatedDate) {
			return true
		}

		return false
	})

	lastIdx := skip + limit

	if lastIdx > total {
		lastIdx = total
	}

	return users[skip:lastIdx], total, tx.Commit()
}

// SearchInGroup method searches users in group.
func (m *Model) SearchInGroup(userContext, groupID int, query string,
	skip, limit int) ([]*User, int, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, 0, err
	}

	var groupusers []*groupsusers.GroupUser
	err = tx.Model(&groupusers).Where("group_id = ?", groupID).Select()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	userIDs := make([]int, len(groupusers))

	for i, v := range groupusers {
		userIDs[i] = v.UserID
	}

	var users []*User
	q := fmt.Sprintf("%%%s%%", query)
	total, err := tx.Model(&users).Where("id in (?)", pg.In(userIDs)).
		Where("is_available = ?", true).
		Where("account_id ilike ? or full_name ilike ?", q, q).
		Order("full_name asc").
		Offset(skip).Limit(limit).SelectAndCount()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	for _, user := range users {
		msg := &messages.Message{
			WasDeleted: false,
		}

		err := tx.Model(msg).
			Where("was_deleted = ?", msg.WasDeleted).
			Where("user_from_id = ? and user_to_id = ?", userContext, user.ID).
			WhereOr("user_to_id = ? and user_from_id = ?", userContext, user.ID).
			Order("created_date desc").Limit(1).Select()

		if err != nil && err != pg.ErrNoRows {
			tx.Rollback()
			return nil, 0, err
		}

		if err == pg.ErrNoRows {
			msg = nil
		}

		user.LastMessage = msg
	}

	return users, total, tx.Commit()
}

// Search method searches users.
func (m *Model) Search(userID int, query string,
	skip, limit int) ([]*User, int, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, 0, err
	}

	group := &groups.Group{
		OwnerID:        userID,
		IsDefaultGroup: true,
	}

	err = tx.Model(group).Where("owner_id = ?", group.OwnerID).
		Where("is_default_group = ?", group.IsDefaultGroup).Select()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	groupID := group.ID
	groupusers := []*groupsusers.GroupUser{}
	err = tx.Model(&groupusers).Where("group_id = ?", groupID).Select()

	if err != nil && err != pg.ErrNoRows {
		tx.Rollback()
		return nil, 0, err
	}

	excludeUserIDs := make([]int, 0, len(groupusers)+1)

	for _, v := range groupusers {
		excludeUserIDs = append(excludeUserIDs, v.UserID)
	}

	excludeUserIDs = append(excludeUserIDs, userID)

	var users []*User
	q := fmt.Sprintf("%%%s%%", query)

	total, err := tx.Model(&users).Where("is_available = ?", true).
		Where("id not in (?)", pg.In(excludeUserIDs)).
		Where("account_id ilike ? or full_name ilike ?", q, q).
		Order("full_name asc").
		Offset(skip).Limit(limit).SelectAndCount()

	if err != nil && err != pg.ErrNoRows {
		tx.Rollback()
		return nil, 0, err
	}

	return users, total, tx.Commit()
}

// Get method gets user by email.
func (m *Model) Get(email string) (*User, error) {

	return nil, nil
}

// Create method creates new user.
func (m *Model) Create(user *User) error {

	return nil
}

// Update method updates user by email.
func (m *Model) Update(email string, user *User) error {

	return nil
}

// Delete method deletes user by email.
func (m *Model) Delete(email string) error {

	return nil
}

// GetDB gets postgres db
func (m *Model) GetDB() *pg.DB {
	return m.pgdb
}
