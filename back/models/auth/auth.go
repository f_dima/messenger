package auth

import (
	"sync"
	"time"
)

// Storage type reperesents auth storage.
type Storage struct {
	m     *sync.RWMutex
	auths map[string]auth
}

type auth struct {
	value   string
	expired int64
}

// NewStorage method creates new auth storage instance.
func NewStorage() (*Storage, error) {
	s := &Storage{
		m:     &sync.RWMutex{},
		auths: make(map[string]auth),
	}

	go func(st *Storage) {
		c := time.Tick(5 * time.Second)
		for range c {
			st.clear()
		}
	}(s)

	return s, nil
}

// Get method gets email by key.
func (s *Storage) Get(key string) (string, bool) {
	s.m.Lock()
	defer s.m.Unlock()

	if auth, ok := s.auths[key]; ok {
		now := time.Now().Unix()

		if now > auth.expired {
			delete(s.auths, key)
			return "", false
		}

		return auth.value, true
	}

	return "", false
}

// Set method sets email and expire by key.
func (s *Storage) Set(key string, value string, expired int64) {
	s.m.Lock()
	s.auths[key] = auth{value, expired}
	s.m.Unlock()
}

// Del method deletes elem by key.
func (s *Storage) Del(key string) {
	s.m.Lock()
	delete(s.auths, key)
	s.m.Unlock()
}

// GetMap method gets all map.
func (s *Storage) GetMap() map[string]string {
	s.m.Lock()
	defer s.m.Unlock()

	m := make(map[string]string, len(s.auths))

	for key, v := range s.auths {
		m[key] = v.value
	}

	return m
}

func (s *Storage) clear() {
	s.m.Lock()
	now := time.Now().Unix()

	for key, auth := range s.auths {
		if now > auth.expired {
			delete(s.auths, key)
		}
	}

	s.m.Unlock()
}
