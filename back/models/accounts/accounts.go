package accounts

import (
	"errors"
	"fmt"
	"messenger/back/models/auth"
	"messenger/back/models/groups"
	"messenger/back/models/users"
	"net/http"
	"net/smtp"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
)

const (
	smtpHost     = "smtp.yandex.ru"
	smtpPort     = 25
	smtpSender   = "messenger.mephi@yandex.ru"
	smtpPassword = "mephi1234"
	cookieExpire = time.Hour * 24 * 7
)

var (
	// ErrExists account exists.
	ErrExists = errors.New("account exists")

	// ErrNotFound account not found.
	ErrNotFound = errors.New("account not found")

	// ErrUnauthorized login data is wrong.
	ErrUnauthorized = errors.New("unauthorized")

	// ErrConflict login data is wrong.
	ErrConflict = errors.New("email conflict")

	// ErrInvalidPass login data is wrong.
	ErrInvalidPass = errors.New("old password is wrong")

	jwtSecret = []byte("my_secret")
)

// Model type reperesents account model.
type Model struct {
	pgdb *pg.DB
	auth *auth.Storage
}

// ModelConfig type reperesents account model config.
type ModelConfig struct {
	Pgdb *pg.DB
	Auth *auth.Storage
}

// Account type represents account
type Account struct {
	Email           string   `json:"email" pg:"email,pk" validate:"required"`
	Password        string   `json:"password,omitempty" pg:"password"`
	OldPassword     string   `json:"old_password,omitempty" pg:"-"`
	FirstName       string   `json:"first_name" pg:"first_name" validate:"required"`
	LastName        string   `json:"last_name,omitempty" pg:"last_name"`
	PhotoURL        string   `json:"photo_url,omitempty" pg:"-"`
	WasDeleted      bool     `json:"-" pg:"was_deleted,use_zero"`
	WasNotConfirmed bool     `json:"-" pg:"was_not_confirmed,use_zero"`
	WasBlocked      bool     `json:"-" pg:"was_blocked,use_zero"`
	IsAdmin         bool     `json:"is_admin,omitempty" pg:"is_admin,use_zero"`
	tableName       struct{} `pg:"accounts"`
}

// LoginData type represents login data
type LoginData struct {
	Email    string
	Password string
}

// NewModel method creates new model instance.
func NewModel(config ModelConfig) (*Model, error) {
	m := &Model{
		pgdb: config.Pgdb,
		auth: config.Auth,
	}

	return m, nil
}

// List method returns accounts list.
func (m *Model) List() ([]*Account, error) {

	return []*Account{&Account{Email: "not_supported"}}, nil
}

// GetCurrentUser method gets account by email.
func (m *Model) GetCurrentUser(userContext int) (*Account, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, err
	}

	user := &users.User{
		ID: userContext,
	}

	err = tx.Model(user).WherePK().Where("is_available = ?", true).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return nil, ErrNotFound
		}

		tx.Rollback()
		return nil, err
	}

	account := &Account{
		Email: user.AccountID,
	}

	err = tx.Model(account).WherePK().Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return nil, ErrNotFound
		}

		tx.Rollback()
		return nil, err
	}

	account.Password = ""
	account.PhotoURL = user.PhotoURL

	return account, tx.Commit()
}

// Get method gets account by email.
func (m *Model) Get(userID int) (*Account, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, err
	}

	user := &users.User{
		ID: userID,
	}

	err = tx.Model(user).WherePK().Where("is_available = ?", true).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return nil, ErrNotFound
		}

		tx.Rollback()
		return nil, err
	}

	account := &Account{
		Email: user.AccountID,
	}

	err = tx.Model(account).WherePK().Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return nil, ErrNotFound
		}

		tx.Rollback()
		return nil, err
	}

	account.PhotoURL = user.PhotoURL

	return account, tx.Commit()
}

// Create method creates new account.
func (m *Model) Create(account *Account) error {
	account.WasNotConfirmed = true

	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	isExist, err := tx.Model(account).WherePK().Exists()

	if err != nil {
		tx.Rollback()
		return err
	}

	if isExist {
		tx.Rollback()
		return ErrExists
	}

	_, err = tx.Model(account).Insert()

	if err != nil {
		tx.Rollback()
		return err
	}

	fullName := account.FirstName

	if account.LastName != "" {
		fullName += " " + account.LastName
	}

	user := &users.User{
		AccountID: account.Email,
		FullName:  fullName,
		PhotoURL:  account.PhotoURL,
	}

	_, err = tx.Model(user).Insert()

	if err != nil {
		tx.Rollback()
		return err
	}

	defaultGroup := &groups.Group{
		Name:           "all",
		OwnerID:        user.ID,
		IsDefaultGroup: true,
	}

	_, err = tx.Model(defaultGroup).Insert()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// UpdateCurrentUser method updates account by email.
func (m *Model) UpdateCurrentUser(userContext int, account *Account) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	user := &users.User{
		ID: userContext,
	}

	err = tx.Model(user).WherePK().Where("is_available = ?", true).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return ErrNotFound
		}

		tx.Rollback()
		return err
	}

	if account.Email != user.AccountID {
		tx.Rollback()
		return ErrConflict
	}

	user.PhotoURL = account.PhotoURL

	_, err = tx.Model(account).
		Set("first_name = ?, last_name = ?",
			account.FirstName, account.LastName).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	fullName := account.FirstName

	if account.LastName != "" {
		fullName += " " + account.LastName
	}

	user.FullName = fullName
	_, err = tx.Model(user).
		Set("full_name = ?, photo_url = ?", user.FullName, user.PhotoURL).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// UpdatePassword method updates account by email.
func (m *Model) UpdatePassword(userContext int, account *Account) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	user := &users.User{
		ID: userContext,
	}

	err = tx.Model(user).WherePK().Where("is_available = ?", true).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return ErrNotFound
		}

		tx.Rollback()
		return err
	}

	acc := &Account{
		Email: user.AccountID,
	}

	err = tx.Model(acc).WherePK().Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return ErrNotFound
		}

		tx.Rollback()
		return err
	}

	if acc.Password != account.OldPassword {
		tx.Rollback()
		return ErrInvalidPass
	}

	acc.Password = account.Password

	_, err = tx.Model(acc).
		Set("password = ?", acc.Password).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// DeleteCurrentUser method deletes account by email.
func (m *Model) DeleteCurrentUser(userContext int) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	user := &users.User{
		ID: userContext,
	}

	err = tx.Model(user).WherePK().Where("is_available = ?", true).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return ErrNotFound
		}

		tx.Rollback()
		return err
	}

	account := &Account{
		Email:      user.AccountID,
		WasDeleted: true,
	}

	_, err = tx.Model(account).
		Set("was_deleted = ?", account.WasDeleted).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	user.IsAvailable = false

	_, err = tx.Model(user).
		Set("is_available = ?", user.IsAvailable).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// Confirm method confirms account by email.
func (m *Model) Confirm(email string) error {
	account := &Account{
		Email: email,
	}

	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	err = tx.Model(account).WherePK().
		Where("was_blocked = ?", false).
		Where("was_deleted = ?", false).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return ErrNotFound
		}

		tx.Rollback()
		return err
	}

	_, err = tx.Model(account).
		Set("was_not_confirmed = ?", false).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	user := &users.User{
		AccountID:   account.Email,
		IsAvailable: true,
	}

	_, err = tx.Model(user).
		Set("is_available = ?", user.IsAvailable).
		Where("account_id = ?", user.AccountID).Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// Login method logins user.
func (m *Model) Login(loginData *LoginData) (*http.Cookie, error) {
	account := &Account{
		Email: loginData.Email,
	}

	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, err
	}

	err = tx.Model(account).WherePK().Where("was_blocked = ?", false).
		Where("was_deleted = ?", false).
		Where("was_not_confirmed = ?", false).
		Where("password = ?", loginData.Password).Select()

	if err != nil {
		if err == pg.ErrNoRows {
			tx.Rollback()
			return nil, ErrUnauthorized
		}

		tx.Rollback()
		return nil, err
	}

	user := &users.User{
		AccountID: account.Email,
	}

	err = tx.Model(user).Where("account_id = ?", user.AccountID).Select()

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	session := uuid.New().String()
	expires := time.Now().Add(cookieExpire)
	m.auth.Set(session, fmt.Sprintf("%d", user.ID), expires.Unix())

	cookie := &http.Cookie{
		Name:    "messenger_session",
		Value:   session,
		Path:    "/",
		Expires: expires,
	}

	return cookie, tx.Commit()
}

// Logout method logouts user.
func (m *Model) Logout(session string) error {
	m.auth.Del(session)

	return nil
}

// Block method updates account by email.
func (m *Model) Block(email string) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	account := &Account{
		Email: email,
	}

	_, err = tx.Model(account).
		Set("was_blocked = ?", true).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	user := &users.User{
		AccountID: email,
	}

	_, err = tx.Model(user).
		Set("is_available = ?", false).
		Where("account_id = ?", user.AccountID).Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Model(user).
		Where("account_id = ?", user.AccountID).Select()

	if err != nil {
		tx.Rollback()
		return err
	}

	sessionMap := m.auth.GetMap()

	for session, userID := range sessionMap {
		if userID == fmt.Sprintf("%d", user.ID) {
			m.auth.Del(session)
		}
	}

	return tx.Commit()
}

// Unblock method updates account by email.
func (m *Model) Unblock(email string) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	account := &Account{
		Email: email,
	}

	_, err = tx.Model(account).
		Set("was_blocked = ?", false).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Model(account).WherePK().Select()

	if err != nil {
		tx.Rollback()
		return err
	}

	if !account.WasNotConfirmed && !account.WasDeleted {
		user := &users.User{
			AccountID: email,
		}

		_, err = tx.Model(user).
			Set("is_available = ?", true).
			Where("account_id = ?", user.AccountID).Update()

		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}

// SetAdmin method updates account by email.
func (m *Model) SetAdmin(email string) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	account := &Account{
		Email: email,
	}

	_, err = tx.Model(account).
		Set("is_admin = ?", true).
		WherePK().Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// AdminSearch method searches users.
func (m *Model) AdminSearch(userID int, query string, blockFilter string,
	skip, limit int) ([]*users.User, int, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, 0, err
	}

	user := &users.User{
		ID: userID,
	}

	err = tx.Model(user).WherePK().Select()

	if err != nil {
		tx.Rollback()
		return nil, 0, err
	}

	var accs []*Account
	q := fmt.Sprintf("%%%s%%", query)
	total := 0

	switch blockFilter {
	case "true":
		total, err = tx.Model(&accs).
			Where("email != ?", user.AccountID).
			Where("email ilike ? or first_name ilike ? or last_name ilike ?", q, q, q).
			Where("was_blocked = ?", true).
			Where("was_deleted = ?", false).
			Where("was_not_confirmed = ?", false).
			Order("first_name asc", "last_name asc").
			Offset(skip).Limit(limit).SelectAndCount()

		if err != nil && err != pg.ErrNoRows {
			tx.Rollback()
			return nil, 0, err
		}
	case "false":
		total, err = tx.Model(&accs).
			Where("email != ?", user.AccountID).
			Where("email ilike ? or first_name ilike ? or last_name ilike ?", q, q, q).
			Where("was_blocked = ?", false).
			Where("was_deleted = ?", false).
			Where("was_not_confirmed = ?", false).
			Order("first_name asc", "last_name asc").
			Offset(skip).Limit(limit).SelectAndCount()

		if err != nil && err != pg.ErrNoRows {
			tx.Rollback()
			return nil, 0, err
		}
	default:
		total, err = tx.Model(&accs).
			Where("email != ?", user.AccountID).
			Where("email ilike ? or first_name ilike ? or last_name ilike ?", q, q, q).
			Where("was_deleted = ?", false).
			Where("was_not_confirmed = ?", false).
			Order("first_name asc", "last_name asc").
			Offset(skip).Limit(limit).SelectAndCount()

		if err != nil && err != pg.ErrNoRows {
			tx.Rollback()
			return nil, 0, err
		}
	}

	resUsers := make([]*users.User, len(accs))

	for i := range accs {
		newUser := &users.User{
			AccountID: accs[i].Email,
		}

		err := tx.Model(newUser).Where("account_id = ?", newUser.AccountID).Select()

		if err != nil {
			tx.Rollback()
			return nil, 0, err
		}

		newUser.IsAdmin = accs[i].IsAdmin
		newUser.WasBlocked = accs[i].WasBlocked
		resUsers[i] = newUser
	}

	return resUsers, total, tx.Commit()
}

// SendEmail sends email with token
func SendEmail(to, token string) error {
	addr := fmt.Sprintf("%s:%d", smtpHost, smtpPort)
	auth := smtp.PlainAuth("", smtpSender, smtpPassword, smtpHost)

	msg := fmt.Sprintf(
		"From: Messenger Mephi <%s>\r\n"+
			"To: %s\r\n"+
			"Subject: Подтверждение электронной почты\r\n"+
			"\r\n"+
			"Пожалуйста, потвердите Ваш адрес электронной почты, перейдя по ссылке: http://localhost/accounts/confirm?token=%s \r\n",
		smtpSender, to, token,
	)

	err := smtp.SendMail(addr, auth, smtpSender, []string{to}, []byte(msg))

	if err != nil {
		return err
	}

	fmt.Printf("Email sent to: %s\n", to)

	return nil
}

// CreateToken creates JWT token from email
func CreateToken(claims jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtSecret)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// GetClaimsFromToken gets claims from JWT token
func GetClaimsFromToken(tokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return jwtSecret, nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}

	return nil, err
}

// GetAuthStorage gets auth storage
func (m *Model) GetAuthStorage() *auth.Storage {
	return m.auth
}
