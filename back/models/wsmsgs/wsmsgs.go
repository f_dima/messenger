package wsmsgs

import (
	"net/http"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

const (
	operationCreate = "create"
	operationDelete = "delete"
	operationInfo   = "info"
)

// Model type reperesents message model.
type Model struct {
	pgdb   *pg.DB
	logger *zap.Logger
	hub    *Hub
}

// ModelConfig type reperesents message model config.
type ModelConfig struct {
	Pgdb   *pg.DB
	Logger *zap.Logger
}

// Wsmsg type represents message
type Wsmsg struct {
	ID            int        `json:"id" pg:"id,pk"`
	UserFromID    int        `json:"user_from_id" pg:"user_from_id" validate:"required"`
	UserToID      int        `json:"user_to_id" pg:"user_to_id" validate:"required"`
	Content       string     `json:"content" pg:"content" validate:"required"`
	CreatedDate   *time.Time `json:"created_date" pg:"created_date"`
	WasDeleted    bool       `json:"-" pg:"was_deleted,use_zero"`
	Operation     string     `json:"operation" pg:"-"`
	StatusMessage string     `json:"status_message" pg:"-"`
	uniqID        string     `json:"-" pg:"-"`
	tableName     struct{}   `pg:"messages"`
}

// NewModel method creates new model instance.
func NewModel(config ModelConfig) (*Model, error) {
	hub := newHub()
	go hub.run()

	m := &Model{
		pgdb:   config.Pgdb,
		logger: config.Logger,
		hub:    hub,
	}

	return m, nil
}

// Create method deletes message by email.
func (m *Model) Create(w http.ResponseWriter,
	r *http.Request, userContext int) error {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return err
	}

	client := &Client{
		hub:    m.hub,
		conn:   conn,
		send:   make(chan *Wsmsg, 256),
		userID: userContext,
		uniqID: uuid.New().String(),
		pgdb:   m.pgdb,
		logger: m.logger,
	}

	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
	return nil
}

// GetDB gets postgres db
func (m *Model) GetDB() *pg.DB {
	return m.pgdb
}
