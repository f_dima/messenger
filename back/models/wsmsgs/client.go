package wsmsgs

import (
	"log"
	"messenger/back/models/groups"
	"messenger/back/models/groupsusers"
	"net/http"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	// newline = []byte{'\n'}
	badResponse = &Wsmsg{StatusMessage: "fail"}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan *Wsmsg

	userID int
	uniqID string
	pgdb   *pg.DB
	logger *zap.Logger
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()

	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

loop:
	for {
		wsmsg := &Wsmsg{}
		err := c.conn.ReadJSON(wsmsg)

		if err != nil {
			c.logger.Error(err.Error())
			break loop
		}

		switch wsmsg.Operation {
		case operationCreate:
			now := time.Now()
			wsmsg.UserFromID = c.userID
			wsmsg.WasDeleted = false
			wsmsg.uniqID = c.uniqID
			wsmsg.CreatedDate = &now
			tx, err := c.pgdb.Begin()

			if err != nil {
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			_, err = tx.Model(wsmsg).Insert()

			if err != nil {
				tx.Rollback()
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			group := &groups.Group{
				OwnerID:        wsmsg.UserToID,
				IsDefaultGroup: true,
			}

			err = tx.Model(group).Where("owner_id = ?", group.OwnerID).
				Where("is_default_group = ?", group.IsDefaultGroup).Select()

			if err != nil {
				tx.Rollback()
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			groupuser := &groupsusers.GroupUser{
				GroupID: group.ID,
				UserID:  wsmsg.UserFromID,
			}

			isExists, err := tx.Model(groupuser).
				Where("group_id = ?", groupuser.GroupID).
				Where("user_id = ?", groupuser.UserID).Exists()

			if err != nil {
				tx.Rollback()
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			if !isExists {
				_, err := tx.Model(groupuser).Insert()

				if err != nil {
					tx.Rollback()
					c.logger.Error(err.Error())
					c.send <- badResponse
					continue loop
				}
			}

			err = tx.Commit()

			if err != nil {
				tx.Rollback()
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			response := &Wsmsg{
				ID:            wsmsg.ID,
				UserFromID:    wsmsg.UserFromID,
				UserToID:      wsmsg.UserToID,
				Content:       wsmsg.Content,
				CreatedDate:   wsmsg.CreatedDate,
				Operation:     operationInfo,
				StatusMessage: "ok",
			}

			c.send <- response
		case operationDelete:
			wsmsg.UserFromID = c.userID
			wsmsg.uniqID = c.uniqID
			tx, err := c.pgdb.Begin()

			if err != nil {
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			isExist, err := tx.Model(wsmsg).
				WherePK().Where("was_deleted = ?", false).Exists()

			if err != nil {
				tx.Rollback()
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			if isExist {
				_, err := tx.Model(wsmsg).Set("was_deleted = ?", true).
					WherePK().Update()

				if err != nil {
					tx.Rollback()
					c.logger.Error(err.Error())
					c.send <- badResponse
					continue loop
				}
			}

			err = tx.Commit()

			if err != nil {
				tx.Rollback()
				c.logger.Error(err.Error())
				c.send <- badResponse
				continue loop
			}

			response := &Wsmsg{
				ID:            wsmsg.ID,
				Operation:     operationInfo,
				StatusMessage: "ok",
			}

			c.send <- response
		default:
			c.logger.Error("invalid operation")
			c.send <- badResponse
			continue loop
		}

		c.hub.broadcast <- wsmsg
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			err := c.conn.WriteJSON(message)

			if err != nil {
				return
			}

			// // Add queued chat messages to the current websocket message.
			// n := len(c.send)
			// for i := 0; i < n; i++ {
			// 	w.Write(newline)
			// 	w.Write(<-c.send)
			// }
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan *Wsmsg, 256)}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
