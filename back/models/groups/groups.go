package groups

import (
	"errors"

	"github.com/go-pg/pg/v10"
)

var (
	// ErrExists group exists.
	ErrExists = errors.New("group exists")

	// ErrNotFound group not found.
	ErrNotFound = errors.New("group not found")
)

// Model type reperesents group model.
type Model struct {
	pgdb *pg.DB
}

// ModelConfig type reperesents group model config.
type ModelConfig struct {
	Pgdb *pg.DB
}

// Group type represents group
type Group struct {
	ID             int      `json:"id" pg:"id,pk"`
	Name           string   `json:"name" pg:"name" validate:"required"`
	OwnerID        int      `json:"owner_id" pg:"owner_id"`
	IsDefaultGroup bool     `json:"is_default_group,omitempty" pg:"is_default_group,use_zero"`
	IsAddAvail     bool     `json:"is_add_avail" pg:"-"`
	tableName      struct{} `pg:"groups"`
}

type groupuser struct {
	ID        int      `pg:"id,pk"`
	GroupID   int      `pg:"group_id"`
	UserID    int      `pg:"user_id"`
	tableName struct{} `pg:"groups_users"`
}

// NewModel method creates new model instance.
func NewModel(config ModelConfig) (*Model, error) {
	m := &Model{
		pgdb: config.Pgdb,
	}

	return m, nil
}

// List method returns groups list.
func (m *Model) List(userContext int, userID *int) ([]*Group, error) {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return nil, err
	}

	var groups []*Group
	err = tx.Model(&groups).Where("owner_id = ?", userContext).
		Order("is_default_group desc").Order("name asc").Select()

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	if len(groups) == 0 || userID == nil {
		return groups, tx.Commit()
	}

	groupIDs := make([]int, len(groups))

	for i, v := range groups {
		groupIDs[i] = v.ID
	}

	var gu []*groupuser
	err = tx.Model(&gu).Where("group_id in (?)", pg.In(groupIDs)).
		Where("user_id = ?", userID).Select()

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	unavailIDs := make(map[int]struct{}, len(gu))

	for _, v := range gu {
		unavailIDs[v.GroupID] = struct{}{}
	}

	for i := range groups {
		if _, ok := unavailIDs[groups[i].ID]; !ok {
			groups[i].IsAddAvail = true
		} else {
			groups[i].IsAddAvail = false
		}
	}

	return groups, tx.Commit()
}

// Get method gets group by email.
func (m *Model) Get(email string) (*Group, error) {

	return nil, nil
}

// Create method creates new group.
func (m *Model) Create(userContext int, group *Group) error {
	group.IsDefaultGroup = false
	group.OwnerID = userContext
	_, err := m.pgdb.Model(group).Insert()

	if err != nil {
		return err
	}

	return nil
}

// Update method updates group by id.
func (m *Model) Update(group *Group) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	isExist, err := tx.Model(group).WherePK().Exists()

	if err != nil {
		tx.Rollback()
		return err
	}

	if !isExist {
		tx.Rollback()
		return ErrNotFound
	}

	_, err = tx.Model(group).WherePK().Set("name = ?", group.Name).Update()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// Delete method deletes group by id.
func (m *Model) Delete(id int) error {
	tx, err := m.pgdb.Begin()

	if err != nil {
		return err
	}

	gu := &groupuser{
		GroupID: id,
	}

	_, err = tx.Model(gu).
		Where("group_id = ?", gu.GroupID).Delete()

	if err != nil {
		tx.Rollback()
		return err
	}

	group := &Group{
		ID: id,
	}

	isExist, err := tx.Model(group).WherePK().Exists()

	if err != nil {
		tx.Rollback()
		return err
	}

	if !isExist {
		tx.Rollback()
		return ErrNotFound
	}

	_, err = tx.Model(group).WherePK().Delete()

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

// GetDB gets postgres db
func (m *Model) GetDB() *pg.DB {
	return m.pgdb
}
