package main

import (
	"log"
	"os"
	"os/signal"

	"gopkg.in/yaml.v2"
)

const (
	env = "MESSENGER_CONFPATH"
)

func init() {
	log.SetFlags(0)
}

func main() {
	log.Println("Started")

	confPath := os.Getenv(env)

	if confPath == "" {
		log.Fatal("path to config not specified")
	}

	conf, err := getConfig(confPath)

	if err != nil {
		log.Fatal(err)
	}

	msgr, err := newMessenger(conf)

	if err != nil {
		log.Fatal(err)
	}

	msgr.Run()
	listenSignals()
	msgr.Stop()
}

func listenSignals() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	s := <-c
	log.Println("Got signal:", s)
}

func getConfig(confPath string) (*messengerConfig, error) {
	file, err := os.Open(confPath)

	if err != nil {
		return nil, err
	}

	defer file.Close()

	var conf messengerConfig
	decoder := yaml.NewDecoder(file)

	err = decoder.Decode(&conf)

	if err != nil {
		return nil, err
	}

	return &conf, nil
}
