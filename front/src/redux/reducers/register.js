import {
  USER_ADD_FINISHED,
  USER_SET_VALIDATED,
  USER_SET_EMAIL,
  USER_SET_PASSWORD,
  USER_SET_FIRSTNAME,
  USER_SET_LASTNAME,
  USER_SHOW_CONFIRM,
  USER_SHOW_DISMISS,
} from '../actions/registerActions';

const initialState = {
  email: "",
  firstName: "",
  lastName: "",
  password: "",
  validated: false,
  token: "",
  showConfirm: false,
  showDismiss: false,
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case USER_SHOW_CONFIRM:
      Object.assign(newState, { showConfirm: action.showConfirm });
      return newState;
    case USER_SHOW_DISMISS:
      Object.assign(newState, { showDismiss: action.showDismiss });
      return newState;
    case USER_SET_VALIDATED:
      Object.assign(newState, { validated: action.validated });
      return newState;
    case USER_SET_EMAIL:
      Object.assign(newState, { email: action.email });
      return newState;
    case USER_SET_PASSWORD:
      Object.assign(newState, { password: action.password });
      return newState;
    case USER_SET_FIRSTNAME:
      Object.assign(newState, { firstName: action.firstname });
      return newState;
    case USER_SET_LASTNAME:
      Object.assign(newState, { lastName: action.lastname });
      return newState;
    case USER_ADD_FINISHED:
      console.log(action);
      Object.assign(newState, { token: action.json });
      return newState;
    default:
      return state;
  }
}