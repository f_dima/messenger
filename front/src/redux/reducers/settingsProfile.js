import {
  SETTINGS_PROFILE_SET_FIRSTNAME_INPUT,
  SETTINGS_PROFILE_SET_LASTNAME_INPUT,
  SETTINGS_PROFILE_SET_SHOW_SUCCESS,
  SETTINGS_PROFILE_SET_SHOW_DISMISS,
  SETTINGS_PROFILE_SET_SHOW_DISMISS_OLD,
} from '../actions/settingsProfileActions';

const initialState = {
  firstNameInput: "",
  lastNameInput: "",
  photo_url_input: "",
  showSuccess: false,
  showDismiss: false,
  showDismissOld: false,
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);
  switch (action.type) {
    case SETTINGS_PROFILE_SET_SHOW_DISMISS:
      Object.assign(newState, { showDismiss: action.value });
      return newState;
    case SETTINGS_PROFILE_SET_SHOW_DISMISS_OLD:
      Object.assign(newState, { showDismissOld: action.value });
      return newState;
    case SETTINGS_PROFILE_SET_SHOW_SUCCESS:
      Object.assign(newState, { showSuccess: action.value });
      return newState;
    case SETTINGS_PROFILE_SET_FIRSTNAME_INPUT:
      Object.assign(newState, { firstNameInput: action.firstName });
      return newState;
    case SETTINGS_PROFILE_SET_LASTNAME_INPUT:
      Object.assign(newState, { lastNameInput: action.lastName });
      return newState;
    default:
      return state;
  }
}
