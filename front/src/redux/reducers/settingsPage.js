import {
  SETTINGS_PAGE_GET_PROFILE,
  SETTINGS_PAGE_GET_GROUP_LIST,
  SETTINGS_PAGE_SET_LOCATION,
} from '../actions/settingsPageActions';

const initialState = {
  email: "",
  firstName: "",
  lastName: "",
  photo_url: "",
  is_admin: false,
  groups: [],
  location: 'user',
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);
  switch (action.type) {
    case SETTINGS_PAGE_SET_LOCATION:
      Object.assign(newState, {location: action.location});
      return newState;
    case SETTINGS_PAGE_GET_GROUP_LIST:
      Object.assign(newState, getResponseGroup(action.json));
      return newState;
    case SETTINGS_PAGE_GET_PROFILE:
      Object.assign(newState, { email: action.json.email, firstName: action.json.first_name, lastName: action.json.last_name, photo_url: action.json.photo_url, is_admin: action.json.is_admin });
      return newState;
    default:
      return state;
  }
}

function getResponseGroup(groups) {
  let res = [];
  for (let i = 0; i < groups.length; ++i) {
    if (groups[i].is_default_group) {
      continue;
    }
    res.push(groups[i]);
  }

  return {
    groups: res,
  }
}