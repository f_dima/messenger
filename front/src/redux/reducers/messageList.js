import {
  MESSAGE_LIST_GET_MESSAGES,
  MESSAGE_LIST_SET_MESSAGES,
  MESSAGE_LIST_SET_ANCHOL_EL,
  MESSAGE_LIST_SET_SHOW_ADD,
  MESSAGE_LIST_SET_SHOW_INFO,
  MESSAGE_LIST_SET_SHOW_DELETE,
  MESSAGE_LIST_SET_FIRST_OPTION,
  MESSAGE_LIST_SET_SECOND_OPTION,
  MESSAGE_LIST_SET_GROUP_LIST,
  MESSAGE_LIST_SET_SELECTED_GROUP,
  MESSAGE_LIST_SET_MESSAGE_INPUT,
} from '../actions/messageListActions';

const initialState = {
  result: [],
  pageNum: 0,
  countPages: 0,
  loadedMessages: [],
  anchorEl: null,
  showAdd: false,
  showDelete: false,
  firstOption: '',
  secondOption: '',
  showInfo: false,
  groupsAddAvail: [],
  groupsNotAdd: [],
  selectedGroups: [],
  messageInput: '',
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case MESSAGE_LIST_SET_MESSAGE_INPUT:
      Object.assign(newState, {messageInput: action.input});
      return newState;
    case MESSAGE_LIST_SET_SELECTED_GROUP:
      Object.assign(newState, {selectedGroups: action.groups});
      return newState;
    case MESSAGE_LIST_SET_GROUP_LIST:
      Object.assign(newState, getResponseGroup(action.groups));
      return newState;
    case MESSAGE_LIST_SET_SHOW_INFO:
      Object.assign(newState, {showInfo: action.showInfo})
      return newState;
    case MESSAGE_LIST_SET_FIRST_OPTION:
      Object.assign(newState, {firstOption: action.option})
      return newState;
    case MESSAGE_LIST_SET_SECOND_OPTION:
      Object.assign(newState, {secondOption: action.option})
      return newState;
    case MESSAGE_LIST_SET_SHOW_DELETE:
      Object.assign(newState, {showDelete: action.showDelete})
      return newState;
    case MESSAGE_LIST_SET_SHOW_ADD:
      Object.assign(newState, {showAdd: action.showAdd})
      return newState;
    case MESSAGE_LIST_SET_ANCHOL_EL:
      Object.assign(newState, {anchorEl: action.value})
      return newState;
    case MESSAGE_LIST_GET_MESSAGES:
      Object.assign(newState, getResponseMessages(action.data, action.pageNum, action.countPages, action.result));
      return newState;
    case MESSAGE_LIST_SET_MESSAGES:
      Object.assign(newState, {result: action.result});
      return newState;
    default:
      return state;
  }
}

function getResponseGroup(groups) {
  let add = [];
  let notAdd = [];
  for (let i = 0; i < groups.length; ++i) {
    if (!groups[i].is_default_group) {
      if (groups[i].is_add_avail) {
        add.push(groups[i]);
      } else {
        notAdd.push(groups[i]);
      }
    }
  }
  return {
    groupsAddAvail: add,
    groupsNotAdd: notAdd,
  }
}

function getResponseMessages(data, pageNum, countPages, result) {
  let res = [];
  for (let i = 0; i < data.length; ++i) {
    res.push({
      id: data[i].id,
      author: data[i].user_from_id,
      message: data[i].content,
      timestamp: data[i].created_date,
    })
  }
  if (result.length === 0) {
    return {
      result: res,
      pageNum: pageNum,
      countPages: countPages,
    }
  }

  return {
    loadedMessages: res,
    pageNum: pageNum,
    countPages: countPages,
  }
}