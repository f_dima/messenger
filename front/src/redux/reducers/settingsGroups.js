import {
  SETTINGS_GROUPS_SET_SHOW_ADD,
  SETTINGS_GROUPS_SET_SHOW_EDIT,
  SETTINGS_GROUPS_SET_SHOW_DELETE,
  SETTINGS_GROUPS_SET_CURRENT_GROUP,
} from '../actions/settingsGroupsActions';

const initialState = {
  showAdd: false,
  showEdit: false,
  showDelete: false,
  currentGroupId: 0,
  currentGroupName: '',
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);
  switch (action.type) {
    case SETTINGS_GROUPS_SET_SHOW_DELETE:
      Object.assign(newState, { showDelete: action.showDelete });
      return newState;
    case SETTINGS_GROUPS_SET_CURRENT_GROUP:
      Object.assign(newState, { currentGroupId: action.id, currentGroupName: action.name });
      return newState;
    case SETTINGS_GROUPS_SET_SHOW_EDIT:
      Object.assign(newState, { showEdit: action.showEdit });
      return newState;
    case SETTINGS_GROUPS_SET_SHOW_ADD:
      Object.assign(newState, { showAdd: action.showAdd });
      return newState;
    default:
      return state;
  }
}