import { 
  CONTACT_LIST_SHOW,
  CONTACT_LIST_GET_GROUP_LIST,
  CONTACT_LIST_SET_GROUP_NAME,
  CONTACT_LIST_SET_CHOOSE_CONTACT,
  CONTACT_LIST_SET_PAGE_NUM,
  CONTACT_LIST_SET_COUNT_PAGES,
  CONTACT_LIST_SET,
 } from '../actions/contactListActions';

const initialState = {
  groupId: -10000,
  groupName: 'Все',
  groupList: [],
  contactList: [],
  contactId: 0,
  contactName: '',
  pageContact: 0,
  cntPagesContact: 0,
  loadedContacts: [],
  photo_url: '',
  email: '',
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case CONTACT_LIST_SET:
      Object.assign(newState, {contactList: action.contactList});
      return newState;
    case CONTACT_LIST_SET_COUNT_PAGES:
      Object.assign(newState, {cntPagesContact: action.countPages});
      return newState;
    case CONTACT_LIST_SET_PAGE_NUM:
      Object.assign(newState, {pageContact: action.pageNum});
      return newState;
    case CONTACT_LIST_SET_CHOOSE_CONTACT:
      Object.assign(newState, {contactId: action.contactId, contactName: action.contactName, photo_url: action.photo_url, email: action.email});
      return newState;
    case CONTACT_LIST_SET_GROUP_NAME:
      Object.assign(newState, {groupName: action.groupName, groupId: action.groupId});
      return newState;
    case CONTACT_LIST_GET_GROUP_LIST:
      Object.assign(newState, getResponseGroup(action.json, action.groupId, action.groupName));
      return newState;
    case CONTACT_LIST_SHOW:
      Object.assign(newState, getResponse(action.json, action.page, action.contactList));
      return newState;
    default:
      return state;
  }
}

function getResponseGroup(groups, groupId, groupName) {
  for (let i = 0; i < groups.length; ++i) {
    if (groups[i].is_default_group) {
      if (groupId === -10000) {
        groupId = groups[i].id;
      }
      if (groupName === '') {
        groupName = 'Все';
      }
      groups[i].name  = 'Все';
      break;
    }
  }

  return {
    groupId: groupId,
    groupList: groups,
    groupName: groupName,
  }
}

function getResponse(data, page, contacts) {
  let contactList = [];

  for (let i = 0; i < data.length; ++i) {
    contactList.push({
      id: data[i].id,
      account_id: data[i].account_id,
      full_name: data[i].full_name,
      photo_url: data[i].photo_url,
      last_message: data[i].last_message ? data[i].last_message.content : '',
    })
  }

  if (page === 1) {
    return {
      contactList: contactList,
    }
  }

  return {
    loadedContacts: contactList,
  }
}