import {
  LOGIN_SET_EMAIL,
  LOGIN_SET_PASSWORD,
  LOGIN_SET_SUCCESSED,
  LOGIN_SHOW_DISMISS,
} from '../actions/loginActions';

const initialState = {
  email: "",
  password: "",
  successed: false,
  showDismiss: false,
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type){
    case LOGIN_SET_EMAIL:
      Object.assign(newState, { email: action.email });
      return newState;
    case LOGIN_SET_PASSWORD:
      Object.assign(newState, { password: action.password });
      return newState;
    case LOGIN_SET_SUCCESSED:
      Object.assign(newState, {successed: action.successed});
      return newState;
    case LOGIN_SHOW_DISMISS:
      Object.assign(newState, {showDismiss: action.showDismiss});
      return newState;
    default:
      return state;
  }
}