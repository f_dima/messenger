import {
  UPLOAD_SET_FILE,
} from '../actions/uploadActions'

const initialState = {
  file: null
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);
  switch (action.type) {
    case UPLOAD_SET_FILE:
      Object.assign(newState, {file: action.file});
      return newState;
    default:
      return state;
  }
}