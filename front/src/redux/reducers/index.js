import { combineReducers } from 'redux';
import contactList from './contactList';
import register from './register';
import confirm from './confirm';
import login from './login';
import searchPage from './searchPage'
import settingsPage from './settingsPage';
import settingsGroups from './settingsGroups';
import messageList from './messageList';
import settingsProfile from './settingsProfile';
import upload from './upload';
import conversationSearch from './conversationSearch';
import adminPage from './adminPage';

export default combineReducers({
  conversationList: contactList,
  register: register,
  confirm: confirm,
  login: login,
  searchPage: searchPage,
  settingsPage: settingsPage,
  settingsGroups: settingsGroups,
  messageList: messageList,
  settingsProfile: settingsProfile,
  upload: upload,
  conversationSearch: conversationSearch,
  adminPage: adminPage,
});