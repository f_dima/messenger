import {
  CONVERSATION_SEARCH_SET_TEXT,
} from '../actions/conversationSearchActions';

const initialState = {
  searchText: "",
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case CONVERSATION_SEARCH_SET_TEXT:
      Object.assign(newState, {searchText: action.text});
      return newState;
    default:
      return state;
  }
}
