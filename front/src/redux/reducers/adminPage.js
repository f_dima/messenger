import {
  ADMIN_PAGE_SET_SEARCH_TEXT,
  ADMIN_PAGE_REQUEST_RESULT,
  ADMIN_PAGE_SET_RESULT,
  ADMIN_PAGE_SET_FILTER,
} from '../actions/adminPageActions';

const initialState = {
  searchText: "",
  result: [],
  pageNum: 0,
  countPages: 0,
  loadedUsers: [],
  filter: 'all',
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case ADMIN_PAGE_SET_SEARCH_TEXT:
      Object.assign(newState, {searchText: action.searchText});
      return newState;
    case ADMIN_PAGE_REQUEST_RESULT:
      Object.assign(newState, getResponse(action.json, action.pageNum, action.countPages, action.result));
      return newState;
    case ADMIN_PAGE_SET_RESULT:
      Object.assign(newState, {result: action.result})
      return newState;
    case ADMIN_PAGE_SET_FILTER:
      Object.assign(newState, {filter: action.filter});
      return newState;
    default:
      return state;
  }
}

function getResponse(json, pageNum, countPages, result) {
  if (result.length === 0) {
    return {
      result: json,
      pageNum: pageNum,
      countPages: countPages,
    }
  }
  return {
    loadedUsers: json,
    pageNum: pageNum,
    countPages: countPages,
  }
}