import {
  SET_CONFIRM_STATUS,
} from '../actions/confirmActions';

const initialState = {
  isValid: false,
}

export default function (state = initialState, action) {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case SET_CONFIRM_STATUS:
      Object.assign(newState, { isValid: action.isValid });
      return newState;
    default:
      return state;
  }
}