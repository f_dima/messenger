import axios from "axios";

export const CONVERSATION_SEARCH_SET_TEXT = 'CONVERSATION_SEARCH_SET_TEXT';

function conversationSearchSetText(text) {
  return {type: CONVERSATION_SEARCH_SET_TEXT, text};
}

export function setSearchText(text) {
  return (dispatch) => {
    dispatch(conversationSearchSetText(text));
  }
}
