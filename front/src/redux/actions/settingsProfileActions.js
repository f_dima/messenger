import axios from "axios";
import {getSettingsProfile} from '../actions/settingsPageActions';

export const SETTINGS_PROFILE_SET_FIRSTNAME_INPUT = 'SETTINGS_PROFILE_SET_FIRSTNAME_INPUT';
export const SETTINGS_PROFILE_SET_LASTNAME_INPUT = 'SETTINGS_PROFILE_SET_LASTNAME_INPUT';
export const SETTINGS_PROFILE_SET_SHOW_SUCCESS = 'SETTINGS_PROFILE_SET_SHOW_SUCCESS';
export const SETTINGS_PROFILE_SET_SHOW_DISMISS_OLD = 'SETTINGS_PROFILE_SET_SHOW_DISMISS_OLD';
export const SETTINGS_PROFILE_SET_SHOW_DISMISS = 'SETTINGS_PROFILE_SET_SHOW_DISMISS';

function settingsProfileSetFirstnameInput(firstName) {
  return {type: SETTINGS_PROFILE_SET_FIRSTNAME_INPUT, firstName};
}

function settingsProfileSetLastnameInput(lastName) {
  return {type: SETTINGS_PROFILE_SET_LASTNAME_INPUT, lastName};
}

function settingsProfileSetShowSuccess(value) {
  return {type: SETTINGS_PROFILE_SET_SHOW_SUCCESS, value};
}

function settingsProfileSetShowDismiss(value) {
  return {type: SETTINGS_PROFILE_SET_SHOW_DISMISS, value};
}

function settingsProfileSetShowDismissOld(value) {
  return {type: SETTINGS_PROFILE_SET_SHOW_DISMISS_OLD, value};
}

export function setShowSuccess(value) {
  return (dispatch) => {
    dispatch(settingsProfileSetShowSuccess(value));
  }
}

export function setShowDismiss(value) {
  return (dispatch) => {
    dispatch(settingsProfileSetShowDismiss(value));
  }
}

export function setShowDismissOld(value) {
  return (dispatch) => {
    dispatch(settingsProfileSetShowDismissOld(value));
  }
}

export function setFirstnameInput(firstName) {
  return (dispatch) => {
    dispatch(settingsProfileSetFirstnameInput(firstName));
  }
}

export function setLastnameInput(lastName) {
  return (dispatch) => {
    dispatch(settingsProfileSetLastnameInput(lastName));
  }
}

export function setNewProfile(email, firstName, lastName, photo_url, file) {
  return async (dispatch) => {
    if (file) {
      const formData = new FormData();
      formData.append( 
        "user-photo", 
        file, 
        file.name 
      );
      await axios.post('/api/users/files', formData)
      .then(async response => {
        console.log(response);
        await axios.put('/api/accounts/current_user/', {
          email: email,
          first_name: firstName,
          last_name: lastName,
          photo_url: response.data,
        }).then(response => {
          console.log(response);
          dispatch(getSettingsProfile());
          dispatch(settingsProfileSetFirstnameInput(''));
          dispatch(settingsProfileSetLastnameInput(''));
        }).catch(error => {
          console.log(error);
          if (error.response && error.response.status === 401) {
            window.location.href = "/login";
          }
        })
      })
      .catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      })
    } else {
      await axios.put('/api/accounts/current_user/', {
        email: email,
        first_name: firstName,
        last_name: lastName,
        photo_url: photo_url,
      }).then(response => {
        console.log(response);
        dispatch(getSettingsProfile());
        dispatch(settingsProfileSetFirstnameInput(''));
        dispatch(settingsProfileSetLastnameInput(''));
      }).catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = "/login";
        }
      })
    }
  }
}

export function setNewPassword(oldPassword, newPassword) {
  return async (dispatch) => {
    await axios.put('/api/accounts/current_user/password', {
      password: newPassword,
      old_password: oldPassword,
    }).then(response => {
      console.log(response);
      dispatch(setShowSuccess(true));
      dispatch(setShowDismissOld(false));
      dispatch(setShowDismiss(false));
    }).catch(error => {
      console.log(error);
      if (error.response) {
        if (error.response.status === 401) {
          window.location.href = '/login';
        } else if (error.response.status === 400) {
          dispatch(setShowDismissOld(true));
        }
      }
    })
  }
}