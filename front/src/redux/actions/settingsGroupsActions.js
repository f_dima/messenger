export const SETTINGS_GROUPS_SET_SHOW_ADD = 'SETTINGS_GROUP_SET_SHOW_ADD';
export const SETTINGS_GROUPS_SET_SHOW_EDIT = 'SETTINGS_GROUP_SET_SHOW_EDIT';
export const SETTINGS_GROUPS_SET_SHOW_DELETE = 'SETTINGS_GROUP_SET_SHOW_DELETE';
export const SETTINGS_GROUPS_SET_CURRENT_GROUP = 'SETTINGS_GROUPS_SET_CURRENT_GROUP';

function setSettingsGroupsShowAdd(showAdd) {
  return {type: SETTINGS_GROUPS_SET_SHOW_ADD, showAdd};
}

function setSettingsGroupsShowEdit(showEdit) {
  return {type: SETTINGS_GROUPS_SET_SHOW_EDIT, showEdit};
}

function setSettingsGroupsShowDelete(showDelete) {
  return {type: SETTINGS_GROUPS_SET_SHOW_DELETE, showDelete};
}

function setSettingsGroupCurrentGroup(id, name) {
  return {type: SETTINGS_GROUPS_SET_CURRENT_GROUP, id, name};
}

export function setShowAddGroup(showAdd) {
  return async (dispatch) => {
    await dispatch(setSettingsGroupsShowAdd(showAdd));
  }
}

export function setShowEditGroup(value) {
  return async (dispatch) => {
    await dispatch(setSettingsGroupsShowEdit(value));
  }
}

export function setShowDeleteGroup(value) {
  return async (dispatch) => {
    await dispatch(setSettingsGroupsShowDelete(value));
  }
}

export function setCurrentGroup(id, name) {
  return async (dispatch) => {
    await dispatch(setSettingsGroupCurrentGroup(id, name));
  }
}
