export const UPLOAD_SET_FILE = 'UPLOAD_SET_FILE';

function uploadSetFile(file) {
  return {type: UPLOAD_SET_FILE, file};
}

export function setFile(file) {
  return (dispatch) => {
    dispatch(uploadSetFile(file));
  }
}