import axios from 'axios';

export const CONTACT_LIST_SHOW = 'CONTACT_LIST_SHOW';
export const CONTACT_LIST_GET_GROUP_LIST = 'CONTACT_LIST_GET_GROUP_LIST';
export const CONTACT_LIST_SET_GROUP_NAME = 'CONTACT_LIST_SET_GROUP_NAME';
export const CONTACT_LIST_SET_CHOOSE_CONTACT = 'CONTACT_LIST_SET_CHOOSE_CONTACT';
export const CONTACT_LIST_SET_PAGE_NUM = 'CONTACT_LIST_SET_PAGE_NUM';
export const CONTACT_LIST_SET_COUNT_PAGES = 'CONTACT_LIST_SET_COUNT_PAGES';
export const CONTACT_LIST_SET = 'CONTACT_LIST_SET';

function contactListSetPageNum(pageNum) {
  return { type: CONTACT_LIST_SET_PAGE_NUM, pageNum };
}

function contactListSetCountPages(countPages) {
  return { type: CONTACT_LIST_SET_COUNT_PAGES, countPages };
}

function contactListShow(json, page, contactList) {
  return { type: CONTACT_LIST_SHOW, json, page, contactList };
}

function contactListSet(contactList) {
  return { type: CONTACT_LIST_SET, contactList };
}

function getGroupListRequest(json, groupId, groupName) {
  return { type: CONTACT_LIST_GET_GROUP_LIST, json, groupId, groupName };
}

function contactListSetGroupName(groupName, groupId) {
  return { type: CONTACT_LIST_SET_GROUP_NAME, groupName, groupId };
}

function contactListSetChooseContact(contactId, contactName, photo_url, email) {
  return { type: CONTACT_LIST_SET_CHOOSE_CONTACT, contactId, contactName, photo_url, email };
}

export function getGroupList(groupId, groupName) {
  return async (dispatch) => {
    await axios.get('/api/groups/')
      .then(async (response) => {
        console.log(response);
        await dispatch(getGroupListRequest(response.data, groupId, groupName));
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      })
  }
}

export function contactListRequest(group_id, pageNum, contactList, search) {
  return async (dispatch) => {
    let url = ''; 
    if (!search) {
      let searchParams = '?group_id=' + group_id;
      if (group_id === -10000) {
        searchParams += '&is_default_group=true';
      }
      url = '/api/users' + searchParams + "&page=" + pageNum + "&per_page=15"
    } else {
      url = '/api/users/search_in_group?group_id='+group_id+"&query="+search+"&page="+pageNum+"&per_page=15";
    }
    await axios.get(url)
      .then((response) => {
        console.log(response);
        let countPages = response.headers["x-total-pages"];
        dispatch(contactListSetCountPages(countPages));
        dispatch(contactListSetPageNum(pageNum));
        dispatch(contactListShow(response.data, pageNum, contactList));
      })
      .catch((error) => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      })
  }
}

export function setGroupName(groupList, groupId) {
  return async (dispatch) => {
    for (let i = 0; i < groupList.length; ++i) {
      if (groupList[i].id == groupId) {
        await dispatch(contactListSetGroupName(groupList[i].name, groupId));
        break;
      }
    }
  }
}

export function setChooseContact(contactId, contactName, photo_url, email) {
  return async (dispatch) => {
    dispatch(contactListSetChooseContact(contactId, contactName, photo_url, email));
  }
}

export function setContactList(contactList) {
  return (dispatch) => {
    dispatch(contactListSet(contactList));
  }
}
