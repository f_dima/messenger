import axios from 'axios';

export const SEARCH_PAGE_SET_SEARCH_TEXT = 'SEARCH_PAGE_SET_SEARCH_TEXT';
export const SEARCH_PAGE_REQUEST_RESULT = 'SEARCH_PAGE_REQUEST_RESULT';
export const SEARCH_PAGE_SET_RESULT = 'SEARCH_PAGE_SET_RESULT';

function searchSetSearchText(searchText) {
  return { type: SEARCH_PAGE_SET_SEARCH_TEXT, searchText };
}

function searchSetResult(result) {
  return {type: SEARCH_PAGE_SET_RESULT, result};
}

function searchRequestRusult(json, pageNum, countPages, result) {
  return { type: SEARCH_PAGE_REQUEST_RESULT, json, pageNum, countPages, result };
}

export function setSearchText(searchText) {
  return async (dispatch) => {
    await dispatch(searchSetSearchText(searchText));
  }
}

export function setSearchResult(result) {
  return async (dispatch) => {
    await dispatch(searchSetResult(result));
  }
}

export function getResultSearch(searchText, pageNum, result) {
  return async (dispatch) => {
    await axios.get('/api/users/search?query='+searchText+'&page='+pageNum+'&per_page=15')
      .then(response => {
        console.log(response);
        let countPages = response.headers["x-total-pages"];
        console.log(countPages);
        dispatch(searchRequestRusult(response.data, pageNum, countPages, result));
      })
      .catch((error) => {
        console.log(error.response);
        if (error.response.status === 401) {
          window.location.href = '/login';
        }
      });
  }
}