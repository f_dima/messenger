import axios from 'axios';

export const SETTINGS_PAGE_GET_PROFILE = 'SETTINGS_PAGE_GET_PROFILE';
export const SETTINGS_PAGE_GET_GROUP_LIST = 'SETTINGS_PAGE_GET_GROUP_LIST';
export const SETTINGS_PAGE_SET_LOCATION = 'SETTINGS_PAGE_SET_LOCATION';
export const SETTINGS_PAGE_ADD_GROUP = 'SETTINGS_PAGE_ADD_GROUP';

function settingsProfileSet(json) {
  return { type: SETTINGS_PAGE_GET_PROFILE, json };
}

function settingsGroupsSet(json) {
  return { type: SETTINGS_PAGE_GET_GROUP_LIST, json };
}

function settingsPageSetLocation(location) {
  return { type: SETTINGS_PAGE_SET_LOCATION, location };
}

export function getSettingsProfile() {
  return async (dispatch) => {
    await axios.get('/api/accounts/current_user/')
      .then(response => {
        console.log(response);
        dispatch(settingsProfileSet(response.data));
      })
      .catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = "/login";
        }
      })
  }
}

export function getGroupList() {
  return async (dispatch) => {
    await axios.get('/api/groups/')
      .then(async (response) => {
        console.log(response);
        await dispatch(settingsGroupsSet(response.data));
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      })
  }
}

export function setLocationSettings(location) {
  return async (dispatch) => {
    await dispatch(settingsPageSetLocation(location));
  }
}

export function addGroup(group) {
  return async (dispatch) => {
    await axios
      .post('/api/groups', {
        name: group,
      }, { "Content-Type": "application/json" })
      .then(response => {
        console.log(response);
        dispatch(getGroupList());
      })
      .catch(error => {
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      });
  }
}

export function editGroup(id, name) {
  return async (dispatch) => {
    await axios
      .put('/api/groups/' + id, {
        id: Number(id),
        name: name,
      })
      .then(response => {
        console.log(response);
        dispatch(getGroupList());
      })
      .catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      })
  }
}

export function deleteGroup(id) {
  return async (dispatch) => {
    await axios({
      method: 'DELETE',
      url: '/api/groups/' + id,
    })
      .then(response => {
        console.log(response);
        dispatch(getGroupList());
      })
      .catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = '/login';
        }
      })
  }
}