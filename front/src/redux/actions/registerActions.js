import axios from 'axios';

export const USER_ADD_FINISHED = 'USER_ADD_FINISHED';
export const USER_SET_VALIDATED = 'USER_SET_VALIDATED';
export const USER_SET_EMAIL = 'USER_SET_EMAIL';
export const USER_SET_PASSWORD = 'USER_SET_PASSWORD';
export const USER_SET_FIRSTNAME = 'USER_SET_FIRSTNAME';
export const USER_SET_LASTNAME = 'USER_SET_LASTNAME';
export const USER_SHOW_CONFIRM = 'USER_SHOW_CONFIRM';
export const USER_SHOW_DISMISS = 'USER_SHOW_DISMISS';

function registerUserAdd (json) {
  return { type: USER_ADD_FINISHED, json };
}

function userSetValidated (validated) {
  return {type: USER_SET_VALIDATED, validated};
}

function userSetEmail (email) {
  return {type: USER_SET_EMAIL, email}
}

function userSetPassword (password) {
  return {type: USER_SET_PASSWORD, password}
}

function userSetFirstname (firstname) {
  return {type: USER_SET_FIRSTNAME, firstname}
}

function userSetLastname (lastname) {
  return {type: USER_SET_LASTNAME, lastname}
}

function userShowConfirm (showConfirm) {
  return {type: USER_SHOW_CONFIRM, showConfirm}
}

function userShowDismiss (showDismiss) {
  return {type: USER_SHOW_DISMISS, showDismiss}
}

export function registerAdd(email, password, firstName, lastName) {
  return async (dispatch) => {
    await axios
      .post('/api/accounts', { 
        email: email,
        password: password,
        first_name: firstName,
        last_name: lastName,
      })
      .then(response => {
        dispatch(registerUserAdd(response.data));
        dispatch(registerShowConfirm(true));
        dispatch(registerShowDismiss(false));
      })
      .catch(error => {
        console.log(error);
        
        if (error.response.status == 409) {
          dispatch(registerShowDismiss(true));
        }

        dispatch(registerShowConfirm(false));
      });
  }
}

export function registerValidated(validated) {
  return async (dispatch) => {
    await dispatch(userSetValidated(validated));
  }
}

export function registerSetEmail(email) {
  return (dispatch) => {
    dispatch(userSetEmail(email));
  }
}

export function registerSetPassword(password) {
  return (dispatch) => {
    dispatch(userSetPassword(password));
  }
}

export function registerSetFirstname(firstname) {
  return (dispatch) => {
    dispatch(userSetFirstname(firstname));
  }
}

export function registerSetLastname(lastname) {
  return (dispatch) => {
    dispatch(userSetLastname(lastname));
  }
}

export function registerShowConfirm(showConfirm) {
  return async (dispatch) => {
    await dispatch(userShowConfirm(showConfirm));
  }
}

export function registerShowDismiss(showDismiss) {
  return async (dispatch) => {
    await dispatch(userShowDismiss(showDismiss));
  }
}