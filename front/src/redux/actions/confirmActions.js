export const SET_CONFIRM_STATUS = 'SET_CONFIRM_STATUS';

function setConfirmStatus(isValid) {
  return {type: SET_CONFIRM_STATUS, isValid}
}

export function setConfirm(isValid) {
  return async (dispatch) => {
    await dispatch(setConfirmStatus(isValid));
  }
}