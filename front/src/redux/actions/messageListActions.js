import axios from 'axios';

import {
  contactListRequest,
  setChooseContact,
} from '../actions/contactListActions';

export const MESSAGE_LIST_GET_MESSAGES = 'MESSAGE_LIST_GET_MESSAGES';
export const MESSAGE_LIST_SET_MESSAGES = 'MESSAGE_LIST_SET_MESSAGES';
export const MESSAGE_LIST_SET_ANCHOL_EL = 'MESSAGE_LIST_SET_ANCHOL_EL';
export const MESSAGE_LIST_SET_SHOW_ADD = 'MESSAGE_LIST_SET_SHOW_ADD';
export const MESSAGE_LIST_SET_SHOW_INFO = 'MESSAGE_LIST_SET_SHOW_INFO';
export const MESSAGE_LIST_SET_SHOW_DELETE = 'MESSAGE_LIST_SET_SHOW_DELETE';
export const MESSAGE_LIST_SET_FIRST_OPTION = 'MESSAGE_LIST_SET_FIRST_OPTION';
export const MESSAGE_LIST_SET_SECOND_OPTION = 'MESSAGE_LIST_SET_SECOND_OPTION';
export const MESSAGE_LIST_SET_GROUP_LIST = 'MESSAGE_LIST_SET_GROUP_LIST';
export const MESSAGE_LIST_SET_SELECTED_GROUP = 'MESSAGE_LIST_SET_SELECTED_GROUP';
export const MESSAGE_LIST_SET_MESSAGE_INPUT = 'MESSAGE_LIST_SET_MESSAGE_INPUT';

function messageListSetMessages(data, pageNum, countPages, result) {
  return { type: MESSAGE_LIST_GET_MESSAGES, data, pageNum, countPages, result };
}

function setMsgs(result) {
  return {type: MESSAGE_LIST_SET_MESSAGES, result};
}

function messageListSetAncholEl(value) {
  return {type: MESSAGE_LIST_SET_ANCHOL_EL, value};
}

function messageListSetShowAdd(showAdd) {
  return {type: MESSAGE_LIST_SET_SHOW_ADD, showAdd};
}

function messageListSetShowInfo(showInfo) {
  return {type: MESSAGE_LIST_SET_SHOW_INFO, showInfo};
}

function messageListSetShowDelete(showDelete) {
  return {type: MESSAGE_LIST_SET_SHOW_DELETE, showDelete};
}

function messageListSetFirstOption(option) {
  return {type: MESSAGE_LIST_SET_FIRST_OPTION, option};
}

function messageListSetSecondOption(option) {
  return {type: MESSAGE_LIST_SET_SECOND_OPTION, option};
}

function messageListSetGroupList(groups) {
  return {type: MESSAGE_LIST_SET_GROUP_LIST, groups};
}

function messageListSetSelectedGroups(groups) {
  return {type: MESSAGE_LIST_SET_SELECTED_GROUP, groups};
}

function messageListSetMessageInput(input) {
  return {type: MESSAGE_LIST_SET_MESSAGE_INPUT, input};
}

export function setMessageInput(input) {
  return (dispatch) => {
    dispatch(messageListSetMessageInput(input));
  }
}

export function setAncholEl(value) {
  return (dispatch) => {
    dispatch(messageListSetAncholEl(value));
  }
}

export function setShowAdd(value) {
  return (dispatch) => {
    dispatch(messageListSetShowAdd(value));
  }
}

export function setShowInfo(value) {
  return (dispatch) => {
    dispatch(messageListSetShowInfo(value));
  }
}

export function setShowDelete(value) {
  return (dispatch) => {
    dispatch(messageListSetShowDelete(value));
  }
}

export function setFirstOption(option) {
  return (dispatch) => {
    dispatch(messageListSetFirstOption(option));
  }
}

export function setSecondOption(option) {
  return (dispatch) => {
    dispatch(messageListSetSecondOption(option));
  }
}

export function setSelectedGroups(groups) {
  return (dispatch) => {
    dispatch(messageListSetSelectedGroups(groups));
  }
}

export function setMessages(result) {
  return async (dispatch) => {
    await dispatch(setMsgs(result));
  }
}

export function getMessages(contactId, pageNum, result) {
  return async (dispatch) => {
    await axios.get('/api/messages/?user_id=' + contactId + '&page=' + pageNum + '&per_page=20')
      .then(response => {
        console.log(response);
        let countPages = response.headers["x-total-pages"];
        dispatch(messageListSetMessages(response.data, pageNum, countPages, result));
      })
      .catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = "/login";
        }
      })
  }
}

export function getGroupList(userId) {
  return async (dispatch) => {
    await axios.get('/api/groups/?user_id='+userId)
    .then(response => {
      console.log(response);
      dispatch(messageListSetGroupList(response.data));
    })
    .catch(error => {
      console.log(error);
      if (error.response && error.response.status === 401) {
        window.location.hres = "/login";
      }
    })
  }
}

export function addContactInGroups(groups, userId) {
  return async (dispatch) => {
    groups.forEach(async group => {
      await axios.post('/api/groupsusers', { 
        user_id: Number(userId),
        group_id: Number(group),
      })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
        if (error.response && error.response.status === 401) {
          window.location.href = "/login";
        }
      })
    })
    dispatch(setSelectedGroups([]));
  }
}

export function deleteContactFromGroup(groupId, contactId, isDefault, contactList, searchText) {
  return async (dispatch) => {
    await axios({
      method: 'DELETE',
      url: '/api/groupsusers', 
      data: {
        user_id: Number(contactId),
        group_id: Number(groupId),
        is_default_group: isDefault,
      },
    })
    .then(response => {
      console.log(response);
      dispatch(contactListRequest(groupId, 1, contactList, searchText));
      dispatch(setChooseContact(0, '', '', ''));
      dispatch(messageListSetMessages([]));
    })
    .catch(error => {
      console.log(error);
      if (error.response && error.response.status === 401) {
        window.location.href = "/login";
      }
    })
  }
}
