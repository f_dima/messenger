import axios from 'axios';

export const ADMIN_PAGE_SET_SEARCH_TEXT = 'ADMIN_PAGE_SET_ADMIN_TEXT';
export const ADMIN_PAGE_REQUEST_RESULT = 'ADMIN_PAGE_REQUEST_RESULT';
export const ADMIN_PAGE_SET_RESULT = 'ADMIN_PAGE_SET_RESULT';
export const ADMIN_PAGE_SET_FILTER = 'ADMIN_PAGE_SET_FILTER';

function adminSetSearchText(searchText) {
  return { type: ADMIN_PAGE_SET_SEARCH_TEXT, searchText };
}

function adminSetResult(result) {
  return {type: ADMIN_PAGE_SET_RESULT, result};
}

function adminSearchRequestRusult(json, pageNum, countPages, result) {
  return { type: ADMIN_PAGE_REQUEST_RESULT, json, pageNum, countPages, result };
}

function adminSetFilter(filter) {
  return {type: ADMIN_PAGE_SET_FILTER, filter};
}

export function setSearchText(searchText) {
  return async (dispatch) => {
    await dispatch(adminSetSearchText(searchText));
  }
}

export function setSearchResult(result) {
  return async (dispatch) => {
    await dispatch(adminSetResult(result));
  }
}

export function getResultSearch(searchText, pageNum, result, block) {
  return async (dispatch) => {
    await axios.get('/api/accounts/admin/search?query='+searchText+'&page='+pageNum+'&per_page=15&block_filter='+block)
      .then(response => {
        console.log(response);
        let countPages = response.headers["x-total-pages"];
        dispatch(adminSearchRequestRusult(response.data, pageNum, countPages, result));
      })
      .catch((error) => {
        console.log(error.response);
        if (error.response.status === 401) {
          window.location.href = '/login';
        }
      });
  }
}

export function setFilter(filter) {
  return async (dispatch) => {
    await dispatch(adminSetFilter(filter));
  }
}