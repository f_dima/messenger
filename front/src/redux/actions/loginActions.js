import axios from 'axios';

export const LOGIN_SET_EMAIL = 'LOGIN_SET_EMAIL';
export const LOGIN_SET_PASSWORD = 'LOGIN_SET_PASSWORD';
export const LOGIN_SET_SUCCESSED = 'LOGIN_SET_SUCCESSED';
export const LOGIN_SHOW_DISMISS = 'LOGIN_SHOW_DISMISS';

function loginSetEmail(email) {
  return {type: LOGIN_SET_EMAIL, email}
}

function loginSetPassword(password) {
  return {type: LOGIN_SET_PASSWORD, password}
}

function loginSetSuccessed(successed) {
  return {type: LOGIN_SET_SUCCESSED, successed}
}

function showDismissLogin(showDismiss) {
  return {type: LOGIN_SHOW_DISMISS, showDismiss}
}

export function loginShowDismiss(showDismiss) {
  return (dispatch) => {
    dispatch(showDismissLogin(showDismiss));
  }
}

export function setLoginEmail(email) {
  return (dispatch) => {
    dispatch(loginSetEmail(email));
  }
}

export function setLoginPassword(password) {
  return (dispatch) => {
    dispatch(loginSetPassword(password));
  }
}

export function checkAuth(email, password) {
  return async (dispatch) => {
  await axios
      .post('/api/accounts/login', { 
        email: email,
        password: password,
      },  { "Content-Type": "application/json" })
      .then(response => {
        console.log(response); 
        dispatch(loginSetSuccessed(true));
        dispatch(loginShowDismiss(false));
      })
      .catch(error => {
        console.log(error);

        if (error.response.status == 401) {
          dispatch(loginShowDismiss(true));
        }

        // dispatch(loginSetSuccessed(false));
      });
    }
}