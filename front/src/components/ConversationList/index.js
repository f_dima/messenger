import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ConversationSearch from '../ConversationSearch';
import ConversationListItem from '../ConversationListItem';
import Toolbar from '../Toolbar';
import ToolbarButton from '../ToolbarButton';
import iosCog from '@iconify/icons-ion/ios-cog';
import iosAddCircleOutline from '@iconify/icons-ion/ios-add-circle-outline';
import { Link } from 'react-router-dom';

import './ConversationList.css';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  groupId: PropTypes.string,
  groupName: PropTypes.string,
  groupList: PropTypes.array,
  contactList: PropTypes.array,
}

class ConversationList extends Component {
  constructor(props) {
    super();
  }

  render() {
    const {
      groupName,
      contactList
    } = this.props;

    return (
      <div className="conversation-list">
        <Toolbar
          title={groupName}
          leftItems={[
            <Link to='/settings'>
              <ToolbarButton key="cog" icon={iosCog} />
            </Link>
          ]}
          rightItems={[
            <Link to='/search'>
              <ToolbarButton key="add" icon={iosAddCircleOutline} />
            </Link>
          ]}
        />
        <ConversationSearch />
        {contactList &&
          contactList.map(conversation =>
            <ConversationListItem
              key={conversation.id}
              data={conversation}
            />
          )
        }
      </div>
    );
  }
}

ConversationList.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    groupId,
    groupName,
    groupList,
    contactList,
  } = (state.conversationList);

  return {
    groupId,
    groupName,
    groupList,
    contactList,
  };
}

export default connect(mapStateToProps)(ConversationList);
