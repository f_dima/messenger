import React, { Component } from 'react';
import { InputGroup, FormControl } from 'react-bootstrap'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from '../Toolbar';
import UploadButton from '../UploadButton';
import './SettingsProfile.css'
import Button from '@material-ui/core/Button';
import Modal from 'react-bootstrap/Modal'
import Alert from 'react-bootstrap/Alert'

import {
  setFirstnameInput,
  setLastnameInput,
  setNewProfile,
  setNewPassword,
  setShowSuccess,
  setShowDismiss,
  setShowDismissOld,
} from '../../redux/actions/settingsProfileActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  firstNameInput: PropTypes.string,
  lastNameInput: PropTypes.string,
  photo_url_input: PropTypes.string,
  showSuccess: PropTypes.bool,
  file: PropTypes.any,
  showDismiss: PropTypes.bool,
  showDismissOld: PropTypes.bool,
}

class SettingsProfile extends Component {
  constructor(props) {
    super();

    this.handleClick = this.handleClick.bind(this);
    this.handleChangeFirstName = this.handleChangeFirstName.bind(this);
    this.handleChangeLastName = this.handleChangeLastName.bind(this);
    this.handleSaveInput = this.handleSaveInput.bind(this);
    this.handleSubmitNewPassword = this.handleSubmitNewPassword.bind(this);
    this.handleCloseSuccess = this.handleCloseSuccess.bind(this);
    this.handleCloseDismiss = this.handleCloseDismiss.bind(this);
    this.handleCloseDismissOld = this.handleCloseDismissOld.bind(this);
  }

  handleClick() {
    window.open(this.props.photo_url);
  }

  handleChangeFirstName(e) {
    this.props.dispatch(setFirstnameInput(e.target.value));
  }

  handleChangeLastName(e) {
    this.props.dispatch(setLastnameInput(e.target.value));
  }

  handleSaveInput() {
    const {
      email,
      firstNameInput,
      firstName,
      lastName,
      lastNameInput,
      photo_url,
      file,
    } = this.props;

    if (!firstNameInput && !lastNameInput && !file) {
      return
    }

    let first = firstNameInput || firstName;
    let last = lastNameInput || lastName;
    this.props.dispatch(setNewProfile(email, first, last, photo_url, file));
  }

  handleSubmitNewPassword() {
    let oldPsw = document.getElementById('old-password').value;
    let newPsw = document.getElementById('new-password').value;
    let retPsw = document.getElementById('retry-new-password').value;
    if (oldPsw && newPsw && retPsw) {
      if (newPsw === retPsw) {
        this.props.dispatch(setNewPassword(oldPsw, newPsw));
      } else {
        this.props.dispatch(setShowDismiss(true));
        document.getElementById('old-password').value = '';
        document.getElementById('new-password').value = '';
        document.getElementById('retry-new-password').value = '';
      }
    }
  }

  handleCloseSuccess() {
    document.getElementById('old-password').value = '';
    document.getElementById('new-password').value = '';
    document.getElementById('retry-new-password').value = '';
    this.props.dispatch(setShowSuccess(false));
  }

  handleCloseDismissOld() {
    this.props.dispatch(setShowDismissOld(false));
  }

  handleCloseDismiss() {
    this.props.dispatch(setShowDismiss(false));
  }

  render() {
    const {
      firstName,
      lastName,
      photo_url,
      firstNameInput,
      lastNameInput,
      showSuccess,
      file,
      showDismiss,
      showDismissOld,
    } = this.props;

    const preview = file ? URL.createObjectURL(file) : '';

    return (
      <div>
        <Toolbar title="Изменить профиль" />
        <div className="settings-profile">
          <img className="settings-user-photo" src={preview || photo_url || '/empty_photo.jpg'} onClick={this.handleClick} />
          <UploadButton />
          <div className="input-name">
            <InputGroup className="input-firstname-lastname">
              <FormControl
                placeholder="Имя"
                aria-label="firstname"
                aria-describedby="basic-addon1"
                value={firstNameInput || firstName}
                onChange={this.handleChangeFirstName}
              />
            </InputGroup>
            <InputGroup className="input-firstname-lastname">
              <FormControl
                placeholder="Фамилия"
                aria-label="lastname"
                aria-describedby="basic-addon1"
                value={lastNameInput || lastName}
                onChange={this.handleChangeLastName}
              />
            </InputGroup>
            <Button className="button-name-submit" variant="contained" onClick={this.handleSaveInput}>Сохранить</Button>
            <div className="input-change-password">
              <h6>Смена пароля</h6>
              <InputGroup>
                <FormControl
                  id="old-password"
                  placeholder="Старый пароль"
                  aria-label="oldPassword"
                  aria-describedby="basic-addon1"
                  type="password"
                />
              </InputGroup>
              <InputGroup>
                <FormControl
                  id="new-password"
                  placeholder="Новый пароль"
                  aria-label="newPassword"
                  aria-describedby="basic-addon1"
                  type="password"
                />
              </InputGroup>
              <InputGroup>
                <FormControl
                  id="retry-new-password"
                  placeholder="Повторите пароль"
                  aria-label="repeatPassword"
                  aria-describedby="basic-addon1"
                  type="password"
                />
              </InputGroup>
              <Button className="button-change-password" variant="contained" onClick={this.handleSubmitNewPassword}>Сменить пароль</Button>
            </div>
            <Alert
                variant="danger"
                onClose={this.handleCloseDismissOld}
                dismissible
                show={showDismissOld}
              >
                <Alert.Heading>Ошибка!</Alert.Heading>
                <p>
                  Старый пароль введен неверно.
                </p>
              </Alert>
              <Alert
                variant="danger"
                onClose={this.handleCloseDismiss}
                dismissible
                show={showDismiss}
              >
                <Alert.Heading>Ошибка!</Alert.Heading>
                <p>
                  Пароли не совпадают.
                </p>
              </Alert>
          </div>
        </div>
        <Modal
          show={showSuccess}
          onHide={this.handleCloseSuccess}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body>
            Пароль успешно изменен!
          </Modal.Body>
          <Modal.Footer>
            <Button variant="contained" onClick={this.handleCloseSuccess}>Закрыть</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

SettingsProfile.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    firstNameInput,
    lastNameInput,
    photo_url_input,
    showSuccess,
    showDismiss,
    showDismissOld,
  } = (state.settingsProfile);

  const {
    file,
  } = (state.upload);

  return {
    firstNameInput,
    lastNameInput,
    photo_url_input,
    showSuccess,
    file,
    showDismiss,
    showDismissOld,
  }
}

export default connect(mapStateToProps)(SettingsProfile);