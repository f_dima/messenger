import React, { Component } from 'react';
import postscribe from 'postscribe';
import md5 from 'md5';

class DonatePage extends Component {
  constructor(props)  {
    super();
  }

  componentDidMount() {
    const login = "messenger-mephi";
    const invoiceId = Math.floor(Math.random() * 2147483646)+1;
    const SignatureValue = md5(login+"::"+invoiceId+":mephi1234");
    const script = '<script type="text/javascript" src="https://auth.robokassa.ru/Merchant/PaymentForm/FormFLS.js?IsTest=1&DefaultSum=10&MerchantLogin='+login+'&InvoiceID='+invoiceId+'&Culture=ru&Encoding=utf-8&SignatureValue='+SignatureValue+'"></script>';
    postscribe('#donat-div', script);
  }

  render() {
    return (
      <div id="donat-div"></div>
    )
  }
}

export default DonatePage; 