import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from 'react-loader-spinner'
import Toolbar from '../Toolbar';
import ToolbarButton from '../ToolbarButton';
import SearchListItem from '../SearchListItem';
import { Link } from 'react-router-dom';
import arrowBackOutline from '@iconify/icons-ion/arrow-back-outline';
import './SearchPage.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  setSearchText,
  getResultSearch,
  setSearchResult,
} from '../../redux/actions/searchPageActions';

let hasMore = false;

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  searchText: PropTypes.string,
  result: PropTypes.array,
  loadedUsers: PropTypes.array,
  pageNum: PropTypes.number,
  countPages: PropTypes.number,
}

class SearchPage extends Component {
  constructor(props) {
    super();

    this.handleSubmitSearch = this.handleSubmitSearch.bind(this);
    this.handleSetSearchText = this.handleSetSearchText.bind(this);
    this.fetchMoreData = this.fetchMoreData.bind(this);
    this.getUsers = this.getUsers.bind(this);

    let url = new URL(window.location.href)
    let searchText = url.searchParams.get('q');
    if (searchText) {
      hasMore = true;
      props.dispatch(setSearchText(searchText));
      setTimeout(() => {
        props.dispatch(getResultSearch(searchText, 1, props.result));
        if (props.result && props.result.length === 0) {
          hasMore = false;
        }
      }, 1000)
    }
  }

  handleSubmitSearch(event) {
    event.preventDefault();
    const { searchText } = this.props;
    let url = new URL(window.location.href);
    let params = new URLSearchParams(url.search);
    params.set('q', searchText);
    window.location.search = params;
  }

  handleSetSearchText(event) {
    this.props.dispatch(setSearchText(event.target.value));
  }

  getUsers() {
    const { pageNum, searchText, result } = this.props;
    this.props.dispatch(getResultSearch(searchText, pageNum + 1, result)).then(() => {
      const { loadedUsers, result } = this.props;
      this.props.dispatch(setSearchResult(result.concat(loadedUsers)));
    })
  }

  fetchMoreData() {
    hasMore = false;
    setTimeout(this.getUsers, 1000)
  }

  render() {
    const {
      searchText,
      pageNum,
      result,
      countPages,
    } = this.props;
    return (
      <div>
        <Toolbar
          title="Поиск"
          leftItems={[
            <Link to='/'>
              <ToolbarButton key="cog" icon={arrowBackOutline} />
            </Link>
          ]}
        />
        <div className="search-main">
          <Paper component="form" className="paper-search">
            <InputBase
              className="input-search"
              placeholder="Введите строку для поиска"
              inputProps={{ 'aria-label': 'search' }}
              onChange={this.handleSetSearchText}
              value={searchText}
            />
            <IconButton type="submit" className="icon-button-search" aria-label="search" onClick={this.handleSubmitSearch}>
              <SearchIcon />
            </IconButton>
          </Paper>
        </div>
        <div className="search-list-div">
            <InfiniteScroll
              dataLength={result ? result.length : 0}
              next={this.fetchMoreData}
              hasMore={pageNum + 1 <= countPages || hasMore }
              loader={<Loader className="search-loader"
                type="ThreeDots"
                color="#00BFFF"
                height={60}
              />}
            >

              {result &&
                result.map(user =>
                  <SearchListItem
                    key={user.account_id}
                    data={user}
                  />
                )}
            </InfiniteScroll>
        </div>
      </div>
    )
  }
}

SearchPage.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    searchText,
    result,
    pageNum,
    countPages,
    loadedUsers,
  } = (state.searchPage);

  return {
    searchText,
    result,
    pageNum,
    countPages,
    loadedUsers,
  }
}

export default connect(mapStateToProps)(SearchPage);