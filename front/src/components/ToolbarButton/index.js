import React from 'react';
import './ToolbarButton.css';
import { Icon } from '@iconify/react';


export default function ToolbarButton(props) {
    const { icon } = props;
    return (
      <Icon className="toolbar-button" icon={icon}></Icon>
    );
}