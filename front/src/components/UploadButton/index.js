import React, { Component } from 'react';
import './UploadButton.css';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  setFile,
} from '../../redux/actions/uploadActions'

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  file: PropTypes.any,
}

class UploadButton extends Component {
  constructor(props) {
    super();

    this.handleChangeFile = this.handleChangeFile.bind(this);
  }

  handleChangeFile(e) {
    this.props.dispatch(setFile(e.target.files[0]))
  }

  render() {

    return (
      <div className="file-div">
        <input
          accept="image/*"
          className="file-input"
          id="contained-button-file"
          type="file"
          onChange={this.handleChangeFile}
        />
        <label htmlFor="contained-button-file">
          <Button variant="contained" color="info" component="span">
            Загрузить
        </Button>
        </label>
      </div>
    );
  }
}

UploadButton.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    file,
  } = (state.upload);

  return {
    file,
  }
}

export default connect(mapStateToProps)(UploadButton);
