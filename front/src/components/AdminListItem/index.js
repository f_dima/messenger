import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import './AdminListItem.css';
import axios from 'axios';
import { BottomNavigation } from '@material-ui/core';

class AdminListItem extends Component {
  constructor(props) {
    super();

    this.handleSetAdmin = this.handleSetAdmin.bind(this);
    this.handleBlocked = this.handleBlocked.bind(this);
    this.handleUnblocked = this.handleUnblocked.bind(this);
  }

  handleUnblocked() {
    const {id, account_id} = this.props.data;
    let old = document.getElementById('unblock-btn'+id);
    let newBtn = document.getElementById('block-btn'+id);
    axios.put('/api/accounts/admin/unblock', {
      email: account_id,
    }).then(response => {
      console.log(response);
      old.style.display = 'none';
      newBtn.style.display = 'block';
    }).catch(error => {
      console.log(error);
      if (error.response && error.response.status === 401) {
        window.location.href = '/login';
      }
    })
  }

  handleBlocked() {
    const {id, account_id} = this.props.data;
    let old = document.getElementById('block-btn'+id);
    let newBtn = document.getElementById('unblock-btn'+id);
    axios.put('/api/accounts/admin/block', {
      email: account_id,
    }).then(response => {
      console.log(response);
      old.style.display = 'none';
      newBtn.style.display = 'block';
    }).catch(error => {
      console.log(error);
      if (error.response && error.response.status === 401) {
        window.location.href = '/login';
      }
    })
  }

  handleSetAdmin() {
    const {id, account_id} = this.props.data;
    let old = document.getElementById("non-admin-btn"+id);
    let newBtn = document.getElementById("admin-btn"+id);
    axios.put('/api/accounts/admin/set_admin', {
      email: account_id,
    }).then(response => {
      console.log(response);
      old.style.display = 'none';
      newBtn.style.display = 'block';
    }).catch(error => {
      console.log(error);
      if (error.response && error.response.status === 401) {
        window.location.href = '/login';
      }
    })
  }

  render() {
    const { id, photo_url, full_name, account_id, was_blocked, is_admin } = this.props.data;

    return (
      <div className="admin-list-item">
        <img className="search-photo" src={photo_url || '/empty_photo.jpg'} alt="search" />
        <div className="search-info">
          <h1 className="search-title">{full_name}</h1>
          <p className="search-snippet">{account_id}</p>
        </div>
        <div className="admin-add-button">
          {!is_admin && 
          <div style={{display: 'inline-block', paddingRight: '10px'}}>
            <Button id={"non-admin-btn"+id} size="small" variant="contained" onClick={this.handleSetAdmin}>Назначить админом</Button>
            <p id={"admin-btn"+id} style={{display: 'none'}}>Админ</p>
          </div>
          }
          {is_admin && 
          <div style={{display: 'inline-block', paddingRight: '10px'}}>
            <Button style={{display: 'none'}} id={"non-admin-btn"+id} size="small" variant="contained" onClick={this.handleSetAdmin}>Назначить админом</Button>
            <p id={"admin-btn"+id}>Админ</p>
          </div>
          }
          {!was_blocked &&
            <div style={{display: 'inline-block'}}>
            <Button size="small" id={"block-btn"+id} variant="contained" color="secondary" onClick={this.handleBlocked}>Заблокировать</Button>
            <Button style={{display: 'none'}} size="small" id={"unblock-btn"+id} variant="contained" color="primary" onClick={this.handleUnblocked}>Разблокировать</Button>
            </div>
          }
          {was_blocked &&
          <div style={{display: 'inline-block'}}>
            <Button size="small" id={"unblock-btn"+id} variant="contained" color="primary" onClick={this.handleUnblocked}>Разблокировать</Button>
            <Button size="small" style={{display: 'none'}} id={"block-btn"+id} variant="contained" color="secondary" onClick={this.handleBlocked}>Заблокировать</Button>
          </div>
          }
        </div>
      </div>
    );
  }
}

export default AdminListItem;