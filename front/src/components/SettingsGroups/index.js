import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { Icon } from '@iconify/react';
import { InputGroup, FormControl } from 'react-bootstrap'
import Modal from 'react-bootstrap/Modal'
import './SettingsGroups.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import closeSharp from '@iconify-icons/ion/close-sharp';

import {
  setShowAddGroup,
  setShowEditGroup,
  setShowDeleteGroup,
  setCurrentGroup,
} from '../../redux/actions/settingsGroupsActions';

import {
  addGroup,
  editGroup,
  deleteGroup,
} from '../../redux/actions/settingsPageActions';

let groupName = "";

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  groups: PropTypes.array,
  showAdd: PropTypes.bool,
  showEdit: PropTypes.bool,
  showDelete: PropTypes.bool,
  currentGroupId: PropTypes.number,
  currentGroupName: PropTypes.string,
}

class SettingsGroups extends Component {
  constructor(props) {
    super();

    this.handleAddGroup = this.handleAddGroup.bind(this);
    this.handleCloseAdd = this.handleCloseAdd.bind(this);
    this.handleChangeAddName = this.handleChangeAddName.bind(this);
    this.handleSubmitAdd = this.handleSubmitAdd.bind(this);
    this.handleCloseEdit = this.handleCloseEdit.bind(this);
    this.handleShowEdit = this.handleShowEdit.bind(this);
    this.handleChangeEditName = this.handleChangeEditName.bind(this);
    this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
    this.handleShowDelete = this.handleShowDelete.bind(this);
    this.handleCloseDelete = this.handleCloseDelete.bind(this);
    this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
  }

  handleAddGroup() {
    this.props.dispatch(setShowAddGroup(true));
  }

  handleCloseAdd() {
    this.props.dispatch(setShowAddGroup(false));
  }

  handleChangeAddName(e) {
    groupName = e.target.value;
  }

  handleSubmitAdd() {
    if (groupName !== "") {
      this.props.dispatch(addGroup(groupName));
      this.props.dispatch(setShowAddGroup(false));
    }
  }

  handleCloseEdit() {
    this.props.dispatch(setShowEditGroup(false));
  }

  handleShowEdit(e) {
    let ind = e.currentTarget.id.indexOf(':');
    let id = e.currentTarget.id.substring(0, ind);
    let name = e.currentTarget.id.substring(ind+1);
    console.log(e.currentTarget.id);
    console.log(name);
    this.props.dispatch(setCurrentGroup(id, name));
    this.props.dispatch(setShowEditGroup(true));
  }

  handleChangeEditName(e) {
    const {currentGroupId} = this.props;
    this.props.dispatch(setCurrentGroup(currentGroupId, e.target.value));
  }

  handleSubmitEdit() {
    const {currentGroupId, currentGroupName} = this.props;
    if (currentGroupName !== "") {
      this.props.dispatch(editGroup(currentGroupId, currentGroupName));
      this.props.dispatch(setShowEditGroup(false));
    }
  }

  handleShowDelete(e) {
    let ind = e.currentTarget.id.indexOf(':');
    let id = e.currentTarget.id.substring(0, ind);
    let name = e.currentTarget.id.substring(ind+1);
    this.props.dispatch(setCurrentGroup(id, name));
    this.props.dispatch(setShowDeleteGroup(true));
  }

  handleCloseDelete() {
    this.props.dispatch(setShowDeleteGroup(false));
  }

  handleSubmitDelete() {
    const {currentGroupId} = this.props;
    this.props.dispatch(deleteGroup(currentGroupId));
    this.props.dispatch(setShowDeleteGroup(false));
  }

  render() {
    const {
      groups,
      showAdd,
      showEdit,
      showDelete,
      currentGroupName,
    } = this.props;

    return (
      <div>
        <div className="settings-group-div">
          <Button className="group-add-btn" color="primary" variant="contained" onClick={this.handleAddGroup}>Добавить</Button>
          {groups &&
            groups.map(group =>
              <div className="settings-group-list-item">
                <div className="settings-group-info">
                  <h1 className="settings-group-title">{group.name}</h1>
                </div>
                <div className="search-add-button">
                  <Button variant="contained" id={group.id+':'+group.name} onClick={this.handleShowEdit}>Редактировать</Button>
                </div>
                <div className="group-delete-div" id={group.id+':'+group.name} onClick={this.handleShowDelete}>
                  <Icon className="group-delete-btn" icon={closeSharp} />
                </div>
              </div>
            )}
        </div>
        <Modal
          show={showAdd}
          onHide={this.handleCloseAdd}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Введите имя группы
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <InputGroup className="input-group-name">
              <FormControl
                placeholder="Имя"
                aria-label="groupname"
                aria-describedby="basic-addon1"
                onChange={this.handleChangeAddName}
              />
            </InputGroup>
          </Modal.Body>
          <Modal.Footer>
          <Button variant="contained" color="primary" onClick={this.handleSubmitAdd}>Подтвердить</Button>
            <Button variant="contained" onClick={this.handleCloseAdd}>Закрыть</Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showEdit}
          onHide={this.handleCloseEdit}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Введите новое имя группы
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <InputGroup className="input-group-name">

              <FormControl
                placeholder="Имя"
                aria-label="groupname"
                aria-describedby="basic-addon1"
                onChange={this.handleChangeEditName}
                value={currentGroupName}
              />
            </InputGroup>
          </Modal.Body>
          <Modal.Footer>
          <Button variant="contained" color="primary" onClick={this.handleSubmitEdit}>Подтвердить</Button>
            <Button variant="contained" onClick={this.handleCloseEdit}>Закрыть</Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showDelete}
          onHide={this.handleCloseDelete}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body>
            {"Вы действительно хотите удалить группу "+currentGroupName+"?"}
          </Modal.Body>
          <Modal.Footer>
          <Button variant="contained" color="secondary" onClick={this.handleSubmitDelete}>Удалить</Button>
            <Button variant="contained" onClick={this.handleCloseDelete}>Закрыть</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

SettingsGroups.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    showAdd,
    showEdit,
    showDelete,
    currentGroupId,
    currentGroupName,
  } = (state.settingsGroups);

  const {
    groups,
  } = (state.settingsPage);

  return {
    groups,
    showAdd,
    showEdit,
    showDelete,
    currentGroupId,
    currentGroupName,
  }
}

export default connect(mapStateToProps)(SettingsGroups);