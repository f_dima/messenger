import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import './SettingsMenu.css';
import ListGroup from 'react-bootstrap/ListGroup'
import Toolbar from '../Toolbar';
import ToolbarButton from '../ToolbarButton';
import { Link } from 'react-router-dom';
import arrowBackOutline from '@iconify/icons-ion/arrow-back-outline';
import { connect } from 'react-redux';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  email: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  photo_url: PropTypes.string,
  groups: PropTypes.array,
  is_admin: PropTypes.bool,
}

class SettingsMenu extends Component {
  constructor(props) {
    super();

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    axios.get('/api/accounts/logout')
      .then(response => {
        console.log(response);
        window.location.href = '/login';
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response);
        }
      })
  }

  render() {
    const {
      email,
      firstName,
      lastName,
      photo_url,
      is_admin,
    } = this.props;
    return (
      <div className="settings-menu">
        <Toolbar
          title="Настройки"
          leftItems={[
            <Link to='/'>
              <ToolbarButton key="cog" icon={arrowBackOutline} />
            </Link>
          ]}
        />
        <Link to='/settings'>
          <div className="settings-menu-item">
            <img className="user-photo" src={photo_url || '/empty_photo.jpg'} />
            <div className="user-info">
              <h1 className="user-title">{firstName + " " + lastName}</h1>
              <p className="user-email">{email}</p>
            </div>
          </div>
        </Link>
        <ListGroup className="settings-list" as="ul">
          <Link to='/settings/groups'>
            <ListGroup.Item action as="li">Управление группами</ListGroup.Item>
          </Link>
          <Link to='/settings/donate'>
            <ListGroup.Item action as="li">Внести пожертвование</ListGroup.Item>
          </Link>
          {is_admin &&
            <Link to='/settings/admin'>
              <ListGroup.Item action as="li">Администрирование</ListGroup.Item>
            </Link>
          }
          <ListGroup.Item onClick={this.handleLogout} action as="li">Выйти</ListGroup.Item>
        </ListGroup>
      </div>
    );
  }
}

SettingsMenu.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    email,
    firstName,
    lastName,
    photo_url,
    groups,
    is_admin,
  } = (state.settingsPage);

  return {
    email,
    firstName,
    lastName,
    photo_url,
    groups,
    is_admin,
  }
}

export default connect(mapStateToProps)(SettingsMenu);
