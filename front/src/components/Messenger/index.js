import React, { Component } from 'react';
import ConversationList from '../ConversationList';
import MessageList from '../MessageList';
import GroupList from '../GroupList';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import './Messenger.css';

import {
  contactListRequest,
  getGroupList,
  setContactList,
} from '../../redux/actions/contactListActions';

import {
  getMessages,
} from '../../redux/actions/messageListActions';

const URL = 'ws://' + window.location.hostname + '/api/ws';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  groupId: PropTypes.string,
  groupName: PropTypes.string,
  groupList: PropTypes.array,
  contactList: PropTypes.array,
  contactId: PropTypes.string,
  cntPagesContact: PropTypes.number,
  pageContact: PropTypes.number,
  loadedContacts: PropTypes.array,
  searchText: PropTypes.string,
}

class Messenger extends Component {
  constructor(props) {
    super();

    this.onSocketOpen = this.onSocketOpen.bind(this);
    this.onSocketData = this.onSocketData.bind(this);
    this.onSocketClose = this.onSocketClose.bind(this);
    this.fetchContacts = this.fetchContacts.bind(this);

    props.dispatch(getGroupList(props.groupId, props.groupName));
    props.dispatch(contactListRequest(props.groupId, 1, props.contactList, props.searchText));

  }

  componentDidMount() {
    this.socket = new WebSocket(URL);
    this.socket.onopen = () => this.onSocketOpen()
    this.socket.onmessage = (m) => this.onSocketData(m)
    this.socket.onclose = () => this.onSocketClose()
  }

  onSocketOpen() {
    console.log('Connection established!')
  }

  onSocketData(message) {
    let decoded = JSON.parse(message.data)
    const { contactId, contactList, searchText } = this.props;
    console.log(contactId);
    if (decoded.operation === "info" && decoded.status_message === "ok") {
      //if (decoded.user_from_id == contactId || decoded.user_to_id == contactId) {
        this.props.dispatch(getMessages(contactId,1,[]));
      //}
      this.props.dispatch(contactListRequest(this.props.groupId, 1, contactList, searchText));
    }
    if (decoded.status_message !== "ok") {
      console.log(decoded);
    }
    if (decoded.operation === "create" || decoded.operation === "delete") {
      //if (decoded.user_from_id == contactId || decoded.user_to_id == contactId) {
        this.props.dispatch(getMessages(contactId,1,[]));
      //}
      this.props.dispatch(contactListRequest(this.props.groupId, 1, contactList, searchText));
    }
  }

  onSocketClose() {
  }

  fetchContacts() {
    const {contactList, groupId, pageContact, cntPagesContact, searchText} = this.props;
    if (pageContact < cntPagesContact) {
      this.props.dispatch(contactListRequest(groupId, pageContact+1, contactList, searchText)).then(() => {
        const {contactList, loadedContacts} = this.props;
        this.props.dispatch(setContactList(contactList.concat(loadedContacts)));
      })
    }
  }

  render() {
    const {
      groupId,
      groupName,
      groupList,
      contactList
    } = this.props;

    return (
      <div className="messenger">

        <div className="scrollable groups">
          <GroupList
            groupList={groupList}
          />
        </div>

        <div id="scroll-div" className="scrollable sidebar">
          <InfiniteScroll
            pageStart={0}
            loadMore={this.fetchContacts}
            hasMore={true || false}
            useWindow={false}
            initialLoad={false}
            threshold={60}
          >
            <ConversationList
              groupId={groupId}
              groupName={groupName}
              groupList={groupList}
              contactList={contactList}
            />
          </InfiniteScroll>
        </div>

        <div className="content">
          <MessageList
            socket={this.socket}
            groupId={groupId}
            groupName={groupName}
          />
        </div>

      </div>
    );
  }
}

Messenger.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    groupId,
    groupName,
    groupList,
    contactList,
    contactId,
    cntPagesContact,
    pageContact,
    loadedContacts,
  } = (state.conversationList);

  const {
    searchText,
  } = (state.conversationSearch);

  return {
    groupId,
    groupName,
    groupList,
    contactList,
    contactId,
    cntPagesContact,
    pageContact,
    loadedContacts,
    searchText,
  };
}

export default connect(mapStateToProps)(Messenger);