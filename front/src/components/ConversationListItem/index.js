import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import shave from 'shave';

import './ConversationListItem.css';

import {
  setChooseContact,
} from '../../redux/actions/contactListActions';

import {
  getMessages,
} from '../../redux/actions/messageListActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
}

class ConversationListItem extends Component {
  constructor(props) {
    super();

    this.handleChooseContact = this.handleChooseContact.bind(this);
  }

  handleChooseContact(e) {
    let ind = e.target.id.indexOf(':');
    let id = e.target.id.substring(0, ind);
    let name = e.target.id.substring(ind+1);
    this.props.dispatch(setChooseContact(id, name, this.props.data.photo_url, this.props.data.account_id));
    this.props.dispatch(getMessages(id, 1, []));
  }
  
  render() {
    const { id, photo_url, full_name, last_message } = this.props.data;

    return (
      <div className="conversation-list-item" id={id+':'+full_name} onClick={this.handleChooseContact}>
        <img className="conversation-photo" id={id+':'+full_name} onClick={this.handleChooseContact} src={photo_url || '/empty_photo.jpg'} alt="conversation" />
        <div className="conversation-info" id={id+':'+full_name} onClick={this.handleChooseContact}>
          <h1 className="conversation-title" id={id+':'+full_name} onClick={this.handleChooseContact}>{full_name}</h1>
          {/* <p className="conversation-status">{ status ? status : '' }</p> */}
          <p className="conversation-snippet" id={id+':'+full_name} onClick={this.handleChooseContact}>{last_message.length > 29 ? last_message.substring(0, 30)+'...' : last_message}</p>
        </div>
      </div>
    );
  }
}

ConversationListItem.propTypes = propTypes;

export default connect(null)(ConversationListItem);
