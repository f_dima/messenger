import React, { Component } from 'react';
import './Login.css'
import TextField from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Alert from 'react-bootstrap/Alert'

import {
  setLoginEmail,
  setLoginPassword,
  checkAuth,
  loginShowDismiss,
} from '../../redux/actions/loginActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  email: PropTypes.string,
  successed: PropTypes.bool,
  password: PropTypes.string,
  showDismiss: PropTypes.bool,
}

class Login extends Component {
  constructor(props) {
    super()

    this.handleRegister = this.handleRegister.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleCloseDismiss = this.handleCloseDismiss.bind(this);
  }

  handleRegister() {
    window.location.href = "/register"
  }

  handleSubmit(e) {
    e.preventDefault();
    const {email, password} = this.props;
    console.log(email, password);
    this.props.dispatch(checkAuth(email, password));
  }

  handleChangeEmail(e) {
    this.props.dispatch(setLoginEmail(e.target.value));
  }

  handleCloseDismiss() {
    this.props.dispatch(loginShowDismiss(false));
  }

  handleChangePassword(e) {
    console.log(e.target.value);
    this.props.dispatch(setLoginPassword(e.target.value));
  }

  render() {
    const {successed, showDismiss} = this.props;
    if (successed) {
      console.log("successed")
      window.location.href = "/";
    }
    return (
      <Card className="login-card">
        <div className="login-page">
          <Avatar className="icon-lock">
            <LockOutlinedIcon />
          </Avatar>
          <form className='login-form' noValidate onSubmit={this.handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Электронная почта"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={this.handleChangeEmail}
            /><TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Пароль"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={this.handleChangePassword}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Войти
          </Button>
            <Grid container>
              <div>
                <Link className="login-link" href="#" variant="body2" onClick={this.handleRegister}>
                  Регистрация
              </Link>
              </div>
            </Grid><Box mt={5}>
            </Box>
          </form>
        </div>
        <Alert 
        variant="danger"
        onClose={this.handleCloseDismiss}
        dismissible
        show={showDismiss}
        >
          <Alert.Heading>Ошибка!</Alert.Heading>
          <p>
            Неверные данные для входа.
          </p>
        </Alert>
      </Card>
    );
  }
}

Login.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    email,
    password,
    successed,
    showDismiss,
  } = (state.login);

  return {
    email,
    password,
    successed,
    showDismiss,
  };
}

export default connect(mapStateToProps)(Login);
