import React, { Component } from 'react';
import Card from 'react-bootstrap/Card'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import Button from 'react-bootstrap/Button'
import {setConfirm} from '../../redux/actions/confirmActions'

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  isValid: PropTypes.bool,
}

class ConfirmPage extends Component {
  constructor(props) {
    super()

    this.handleLogin = this.handleLogin.bind(this);

    let path = window.location.pathname;
    let search = window.location.search;
    axios.get('/api' + path + search) // TODO location.hostname
      .then(response => {
        props.dispatch(setConfirm(true));
        console.log(response);
        //window.location.href = "/login"
      })
  }

  handleLogin() {
    window.location.href = "/login";
  }

  render() {
    const { isValid } = this.props;
    console.log(isValid)
    return (
      <Card>
        <Card.Header as="h5">MEPHI Messenger</Card.Header>
        <Card.Body>
          {isValid &&
            <div>
              <Card.Text>
                Аккаунт успешно подтверждён
            </Card.Text>
              <Button variant="primary" onClick={this.handleLogin}>Авторизоваться</Button>
            </div>
          }
          {!isValid &&
            <Card.Text>
              При подтверждении аккаунта произошла ошибка
          </Card.Text>
          }
        </Card.Body>
      </Card>
    )
  }
}

ConfirmPage.propTypes = propTypes;

function mapStateToProps(state) {
  const {isValid} = (state.confirm);
  return {isValid}
}

export default connect(mapStateToProps)(ConfirmPage);