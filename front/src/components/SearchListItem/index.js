import React, { Component } from 'react';
import iosAddCircleOutline from '@iconify/icons-ion/ios-add-circle-outline';
import ToolbarButton from '../ToolbarButton';
import axios from 'axios';
import './SearchListItem.css';

class SearchListItem extends Component {
  constructor(props) {
    super();

    this.handleAddUser = this.handleAddUser.bind(this);
  }

  handleAddUser(e) {
    const {id} = this.props.data;
    let tag = e.currentTarget;
    axios
      .post('/api/groupsusers/', { 
        group_id: -1,
        user_id: id,
        is_default_group: true,
      },  { "Content-Type": "application/json" })
      .then((response) => {
        tag.remove();
      })
      .catch(error => {
        console.log(error)
        if (error.response && error.response.status === 401) {
          window.location.href = "/login";
        }
      });
  }

  render() {
    const { photo_url, full_name, account_id } = this.props.data;

    return (
      <div className="search-list-item">
        <img className="search-photo" src={photo_url || '/empty_photo.jpg'} alt="search" />
        <div className="search-info">
          <h1 className="search-title">{full_name}</h1>
          <p className="search-snippet">{account_id}</p>
        </div>
        <div className="search-add-button" onClick={this.handleAddUser}>
          <ToolbarButton key="add" icon={iosAddCircleOutline} />
        </div>
      </div>
    );
  }
}

export default SearchListItem;