import React, { Component } from 'react';
import Compose from '../Compose';
import Toolbar from '../Toolbar';
import ToolbarButton from '../ToolbarButton';
import Message from '../Message';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import arrowUpCircle from '@iconify/icons-ion/arrow-up-circle';
import './MessageList.css';
import informationCircleOutline from '@iconify-icons/ion/information-circle-outline';
import Modal from 'react-bootstrap/Modal'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import InfiniteScroll from 'react-infinite-scroll-component';

import {
  setAncholEl,
  setShowAdd,
  setShowDelete,
  setFirstOption,
  setSecondOption,
  setShowInfo,
  getGroupList,
  setSelectedGroups,
  addContactInGroups,
  deleteContactFromGroup,
  getMessages,
  setMessages,
} from '../../redux/actions/messageListActions';

let hasMore = false;

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  contactId: PropTypes.number,
  contactName: PropTypes.string,
  loadedMessages: PropTypes.array,
  result: PropTypes.array,
  pageNum: PropTypes.number,
  countPages: PropTypes.number,
  anchorEl: PropTypes.any,
  groupId: PropTypes.number,
  groupName: PropTypes.string,
  showAdd: PropTypes.bool,
  showDelete: PropTypes.bool,
  firstOption: PropTypes.string,
  secondOption: PropTypes.string,
  photo_url: PropTypes.string,
  email: PropTypes.string,
  showInfo: PropTypes.bool,
  groupsAddAvail: PropTypes.array,
  groupsNotAdd: PropTypes.array,
  selectedGroups: PropTypes.array,
  messageInput: PropTypes.string,
  contactList: PropTypes.array,
  searchText: PropTypes.string,
}

const ITEM_HEIGHT = 28;

const MY_USER_ID = 'apple';

class MessageList extends Component {
  constructor(props) {
    super();

    this.renderMessages = this.renderMessages.bind(this);
    this.handleClickOptions = this.handleClickOptions.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleAddModal = this.handleAddModal.bind(this);
    this.handleDeleteModal = this.handleDeleteModal.bind(this);
    this.handleCloseAdd = this.handleCloseAdd.bind(this);
    this.handleCloseDelete = this.handleCloseDelete.bind(this);
    this.handleShowInfo = this.handleShowInfo.bind(this);
    this.handleCloseInfo = this.handleCloseInfo.bind(this);
    this.handleSelectGroup = this.handleSelectGroup.bind(this);
    this.handleSubmitAdd = this.handleSubmitAdd.bind(this);
    this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.handleOpenPhoto = this.handleOpenPhoto.bind(this);
    this.fetchMoreData = this.fetchMoreData.bind(this);
  }

  renderMessages() {
    const { 
      loadedMessages, 
      contactId,
      pageNum,
      result,
      countPages,
    } = this.props;

    let messageCount = result.length;
    let res = [];
    let i = messageCount - 1;

    while (i >= 0) {
      let previous = result[i + 1];
      let current = result[i];
      let next = result[i - 1];
      let isMine = current.author != contactId;
      let currentMoment = moment(current.timestamp);
      let prevBySameAuthor = false;
      let nextBySameAuthor = false;
      let startsSequence = true;
      let endsSequence = true;
      let showTimestamp = true;

      if (previous) {
        let previousMoment = moment(previous.timestamp);
        let previousDuration = moment.duration(currentMoment.diff(previousMoment));
        prevBySameAuthor = previous.author === current.author;

        if (prevBySameAuthor && previousDuration.as('hours') < 1) {
          startsSequence = false;
        }

        if (previousDuration.as('hours') < 1) {
          showTimestamp = false;
        }
      }

      if (next) {
        let nextMoment = moment(next.timestamp);
        let nextDuration = moment.duration(nextMoment.diff(currentMoment));
        nextBySameAuthor = next.author === current.author;

        if (nextBySameAuthor && nextDuration.as('hours') < 1) {
          endsSequence = false;
        }
      }

      res.push({
        key: i,
        isMine: isMine,
        startsSequence: startsSequence,
        endsSequence: endsSequence,
        showTimestamp: showTimestamp,
        data: current,
      })

      // Proceed to the next message.
      i -= 1;
    }

    res = res.reverse();

    return (
      <InfiniteScroll
        dataLength={res ? res.length : 0}
        inverse={true}
        next={this.fetchMoreData}
        hasMore={pageNum + 1 <= countPages || hasMore }
        style={{ display: "flex", flexDirection: "column-reverse" }}
        scrollableTarget="scrollableDiv"
      >
        {res.map(message =>
          <Message
            key={message.key}
            isMine={message.isMine}
            startsSequence={message.startsSequence}
            endsSequence={message.endsSequence}
            showTimestamp={message.showTimestamp}
            data={message.data}
            socket={this.props.socket}
            contactId={this.props.contactId}
          />
        )
        }
      </InfiniteScroll>
    )

  }

  handleClickOptions(e) {
    this.props.dispatch(setAncholEl(e.currentTarget));
  }

  handleClose() {
    this.props.dispatch(setAncholEl(null));
  }

  handleAddModal() {
    const { contactId } = this.props;
    this.props.dispatch(getGroupList(contactId));
    this.props.dispatch(setAncholEl(null));
    this.props.dispatch(setShowAdd(true));
  }

  handleDeleteModal() {
    this.props.dispatch(setAncholEl(null));
    this.props.dispatch(setShowDelete(true));
  }

  handleCloseAdd() {
    this.props.dispatch(setSelectedGroups([]));
    this.props.dispatch(setShowAdd(false));
  }

  handleCloseDelete() {
    this.props.dispatch(setShowDelete(false));
  }

  handleShowInfo() {
    this.props.dispatch(setShowInfo(true));
  }

  handleCloseInfo() {
    this.props.dispatch(setShowInfo(false));
  }

  handleSelectGroup(e) {
    const { selectedGroups } = this.props;
    let res = [];
    if (e.target.checked) {
      res = selectedGroups;
      res.push(e.target.name);
    } else {
      res = selectedGroups.filter(group => group != e.target.name);
    }
    console.log(res);
    this.props.dispatch(setSelectedGroups(res));
  }

  handleSubmitAdd() {
    const { selectedGroups, contactId } = this.props;
    this.props.dispatch(addContactInGroups(selectedGroups, contactId));
    this.props.dispatch(setShowAdd(false));
  }

  handleSubmitDelete() {
    const {
      groupId,
      groupName,
      contactId,
      contactList,
      searchText,
    } = this.props;

    if (groupName === 'Все') {
      this.props.dispatch(deleteContactFromGroup(groupId, contactId, true, contactList, searchText));
    } else {
      this.props.dispatch(deleteContactFromGroup(groupId, contactId, false, contactList, searchText));
    }
    this.props.dispatch(setShowDelete(false));
  }

  handleSendMessage(e) {
    if (document.getElementById('input-message').value) {
      console.log(this.props.messageInput);
      this.props.socket.send(JSON.stringify({
        user_to_id: Number(this.props.contactId),
        content: this.props.messageInput,
        operation: "create",
      }))

      this.forceUpdate()
      let input = document.getElementById('input-message');
      console.log(input);
      input.value = '';
    }
  }

  handleOpenPhoto() {
    window.open(this.props.photo_url);
  }

  fetchMoreData() {
    hasMore = false;
    const { contactId, pageNum, result } = this.props;
    this.props.dispatch(getMessages(contactId, pageNum + 1, result)).then(() => {
      const { loadedMessages, result } = this.props;
      this.props.dispatch(setMessages(result.concat(loadedMessages)));
    })
  }

  render() {
    const {
      contactName,
      anchorEl,
      groupName,
      groupsAddAvail,
      groupsNotAdd,
      showAdd,
      showDelete,
      firstOption,
      secondOption,
      photo_url,
      email,
      showInfo,
    } = this.props;

    console.log(this.props);

    if (groupName === 'Все') {
      this.props.dispatch(setFirstOption("Добавить в группу"));
      this.props.dispatch(setSecondOption("Удалить из контактов"));
    } else {
      this.props.dispatch(setFirstOption(""));
      this.props.dispatch(setSecondOption("Удалить из группы"));
    }

    let deleteString = "Вы уверены, что хотите удалить пользователя " + contactName;

    return (
      <div className="message-list" >
        {contactName &&
          <Toolbar
            title={contactName}
            rightItems={[
              <div>
                <IconButton
                  className="icon-button-opt"
                  aria-label="more"
                  aria-controls="long-menu"
                  aria-haspopup="true"
                  onClick={this.handleClickOptions}
                >
                  <MoreVertIcon />
                </IconButton>
                <Menu
                  id="long-menu"
                  keepMounted
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={this.handleClose}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: '20ch',
                    },
                  }}
                >
                  {firstOption &&
                    <MenuItem key={firstOption} onClick={this.handleAddModal}>
                      {firstOption}
                    </MenuItem>
                  }
                  {secondOption &&
                    <MenuItem className="delete-option" key={secondOption} onClick={this.handleDeleteModal}>
                      {secondOption}
                    </MenuItem>
                  }
                </Menu>
              </div>,
              <div onClick={this.handleShowInfo}>
                <ToolbarButton key="info" icon={informationCircleOutline} />
              </div>
              // <ToolbarButton key="phone" icon="ion-ios-call" />
            ]}
          />
        }

        <div id="scrollableDiv" className="message-list-container">
          {this.renderMessages()}
        </div>
        {contactName &&
          <Compose rightItems={[
            <div onClick={this.handleSendMessage}>
              <ToolbarButton key="arrow" icon={arrowUpCircle} />
            </div>,
          ]} />
        }
        <Modal
          show={showAdd}
          onHide={this.handleCloseAdd}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Выберите группу
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {groupsNotAdd.length === 0 && groupsAddAvail.length === 0 ? "Нет доступных групп" : ""}
            <FormGroup row>
              {groupsNotAdd &&
                groupsNotAdd.map(group =>
                  <FormControlLabel
                    disabled={!group.is_add_avail}
                    control={<Checkbox checked={!group.is_add_avail} name={group.id} />}
                    label={group.name}
                  />
                )
              }
              {groupsAddAvail &&
                groupsAddAvail.map(group =>
                  <FormControlLabel
                    control={<Checkbox onChange={this.handleSelectGroup} name={group.id} />}
                    label={group.name}
                  />
                )
              }
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            {groupsNotAdd.length !== 0 || groupsAddAvail.length !== 0 &&
              <Button variant="contained" color="primary" onClick={this.handleSubmitAdd}>Подтвердить</Button>
            }
            <Button variant="contained" onClick={this.handleCloseAdd}>Закрыть</Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showDelete}
          onHide={this.handleCloseDelete}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body>
            {groupName === 'Все' ? deleteString + " из списка контактов?" : deleteString + " из группы " + groupName + "?"}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="contained" color="secondary" onClick={this.handleSubmitDelete}>Удалить</Button>
            <Button variant="contained" onClick={this.handleCloseDelete}>Закрыть</Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showInfo}
          onHide={this.handleCloseInfo}
          size="sm"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          className="info-modal"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              <div className="wrapping-fields">{contactName}</div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img className="info-photo" src={photo_url || '/empty_photo.jpg'} onClick={this.handleOpenPhoto} />
            <p className="wrapping-fields">{email}</p>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

MessageList.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    anchorEl,
    showAdd,
    showDelete,
    firstOption,
    secondOption,
    showInfo,
    groupsAddAvail,
    groupsNotAdd,
    selectedGroups,
    messageInput,
    result,
    pageNum,
    countPages,
    loadedMessages,
  } = (state.messageList);

  const {
    contactId,
    contactName,
    groupId,
    groupName,
    photo_url,
    email,
    contactList,
  } = (state.conversationList);

  const {
    searchText,
  } = (state.conversationSearch);

  return {
    contactId,
    contactName,
    anchorEl,
    groupId,
    groupName,
    showAdd,
    showDelete,
    firstOption,
    secondOption,
    photo_url,
    email,
    showInfo,
    groupsAddAvail,
    groupsNotAdd,
    selectedGroups,
    messageInput,
    contactList,
    searchText,
    result,
    pageNum,
    countPages,
    loadedMessages,
  }
}

export default connect(mapStateToProps)(MessageList);