import React, { Component } from 'react';
import './GroupList.css'
import { Icon } from '@iconify/react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import folderOpenOutline from '@iconify/icons-ion/folder-open-outline';

import {
  contactListRequest,
  setGroupName,
} from '../../redux/actions/contactListActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  groupId: PropTypes.string,
  groupList: PropTypes.array,
  contactList: PropTypes.array,
  searchText: PropTypes.string,
}

class GroupList extends Component {
  constructor(props) {
    super();
    this.handleChangeGroup = this.handleChangeGroup.bind(this);
  }

  handleChangeGroup(e) {
    const {groupList, contactList, searchText} = this.props;
    if (e.target.id && e.target.id !== this.props.groupId) {
      this.props.dispatch(setGroupName(groupList, e.target.id))
      this.props.dispatch(contactListRequest(e.target.id, 1, contactList, searchText));
    }
  }

  render() {
    const { groupList } = this.props;
    return (
      <div className="group-list">
        {
          groupList.map((group) => (
            <div className="list-item">
              <Icon className="group-button" id={group.id} icon={folderOpenOutline} onClick={this.handleChangeGroup} />
              <div className="group-label">{group.name}</div>
            </div>
          ))}
      </div>
    );
  }
}

GroupList.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    groupId,
    groupList,
    contactList,
  } = (state.conversationList);

  const {
    searchText,
  } = (state.conversationSearch);

  return {
    groupId,
    groupList,
    contactList,
    searchText,
  };
}

export default connect(mapStateToProps)(GroupList);