import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Compose.css';

import {
  setMessageInput,
} from '../../redux/actions/messageListActions';

class Compose extends Component {
  constructor(props) {
    super();

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.dispatch(setMessageInput(e.target.value));
  }

  render() {
    return (
      <div className="compose">
        <textarea
          id="input-message"
          type="text"
          className="compose-input"
          placeholder="Введите сообщение"
          rows="1"
          onChange={this.handleChange}
        />

        {
          this.props.rightItems
        }
      </div>
    );
  }
}

export default connect(null)(Compose);