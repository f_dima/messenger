import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from 'react-loader-spinner';
import AdminListItem from '../AdminListItem';
import './AdminPage.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  setFilter,
  setSearchResult,
  setSearchText,
  getResultSearch,
} from '../../redux/actions/adminPageActions';

let hasMore = false;
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

let value = 'Все';

const filters = [
  'Все',
  'Заблокированные',
  'Не заблокированные',
]

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  searchText: PropTypes.string,
  result: PropTypes.array,
  loadedUsers: PropTypes.array,
  pageNum: PropTypes.number,
  countPages: PropTypes.number,
  filter: PropTypes.string,
}

class AdminPage extends Component {
  constructor(props) {
    super()

    this.handleSetSearchText = this.handleSetSearchText.bind(this);
    this.handleSubmitSearch = this.handleSubmitSearch.bind(this);
    this.handleChangeFilter = this.handleChangeFilter.bind(this);
    this.fetchMoreData = this.fetchMoreData.bind(this);
    this.getUsers = this.getUsers.bind(this);

    let url = new URL(window.location.href)
    let searchText = url.searchParams.get('q');
    let block = url.searchParams.get('block');
    if (searchText) {
      hasMore = true;
      props.dispatch(setSearchText(searchText));
      if (block) {
        props.dispatch(setFilter(block));
        if (block === 'all') {
          value = 'Все';
        } else if (block === 'true') {
          value = 'Заблокированные';
        } else if (block === 'false') {
          value = 'Не заблокированные';
        }
      }
      setTimeout(() => {
        props.dispatch(getResultSearch(searchText, 1, props.result, block));
        if (props.result && props.result.length === 0) {
          hasMore = false;
        }
      }, 500)
    }
  }

  handleSetSearchText(event) {
    this.props.dispatch(setSearchText(event.target.value));
  }

  handleSubmitSearch(event) {
    event.preventDefault();
    const { searchText, filter } = this.props;
    let url = new URL(window.location.href);
    let params = new URLSearchParams(url.search);
    params.set('q', searchText);
    params.set('block', filter);
    window.location.search = params;
  }

  handleChangeFilter(e) {
    if (e.target.value === 'Все') {
      this.props.dispatch(setFilter('all'));
      value = 'Все';
    } else if (e.target.value === 'Заблокированные') {
      this.props.dispatch(setFilter('true'));
      value = 'Заблокированные';
    } else if (e.target.value === 'Не заблокированные') {
      this.props.dispatch(setFilter('false'));
      value = 'Не заблокированные';
    }
  }

  getUsers() {
    const { pageNum, searchText, result, filter } = this.props;
    this.props.dispatch(getResultSearch(searchText, pageNum + 1, result, filter)).then(() => {
      const { loadedUsers, result } = this.props;
      this.props.dispatch(setSearchResult(result.concat(loadedUsers)));
    })
  }

  fetchMoreData() {
    hasMore = false;
    setTimeout(this.getUsers, 500)
  }

  render() {
    const {
      searchText,
      pageNum,
      result,
      countPages,
      filter,
    } = this.props;

    return (
      <div>
        <div className="admin-main">
          <Paper component="form" className="paper-search">
            <InputBase
              className="input-search"
              placeholder="Введите строку для поиска"
              inputProps={{ 'aria-label': 'search' }}
              onChange={this.handleSetSearchText}
              value={searchText}
            />
            <IconButton type="submit" className="icon-button-search" aria-label="search" onClick={this.handleSubmitSearch}>
              <SearchIcon />
            </IconButton>
          </Paper>
        </div>
        <div className="select-filter">
        <FormControl>
          <Select
            displayEmpty
            value={value}
            onChange={this.handleChangeFilter}
            input={<Input />}
            renderValue={(selected) => {
              return <em>{selected}</em>;
            }}
            MenuProps={MenuProps}
            inputProps={{ 'aria-label': 'Without label' }}
          >
            {filters.map((f) => (
              <MenuItem key={f} value={f} >
                {f}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        </div>
        <div className="admin-list-div">
            <InfiniteScroll
              dataLength={result ? result.length : 0}
              next={this.fetchMoreData}
              hasMore={pageNum + 1 <= countPages || hasMore }
              // loader={<Loader className="search-loader"
              //   type="ThreeDots"
              //   color="#00BFFF"
              //   height={60}
              // />}
              scrollableTarget="scrollable-container"
            >
              <div id="scrollable-container" className="search-list-container">
              {result &&
                result.map(user =>
                  <AdminListItem
                    key={user.account_id}
                    data={user}
                  />
                )}
                </div>
            </InfiniteScroll>
        </div>
      </div>
    )
  }
}

AdminPage.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    searchText,
    result,
    pageNum,
    countPages,
    loadedUsers,
    filter,
  } = (state.adminPage);

  return {
    searchText,
    result,
    pageNum,
    countPages,
    loadedUsers,
    filter,
  }
}

export default connect(mapStateToProps)(AdminPage);