import React, { Component } from 'react';
import moment from 'moment';
import { Icon } from '@iconify/react';
import closeSharp from '@iconify-icons/ion/close-sharp';
import './Message.css';

class Message extends Component {
  constructor(props) {
    super();

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }

  handleDeleteClick() {
    console.log(this.props.data);
    this.props.socket.send(JSON.stringify({
      id: Number(this.props.data.id),
      operation: "delete",
      user_to_id: Number(this.props.contactId),
    }))
    this.forceUpdate()
  }

  render() {
    const {
      data,
      isMine,
      startsSequence,
      endsSequence,
      showTimestamp
    } = this.props;

    const friendlyTimestamp = moment(data.timestamp).format('LLLL');
    return (
      <div className={[
        'message',
        `${isMine ? 'mine' : ''}`,
        `${startsSequence ? 'start' : ''}`,
        `${endsSequence ? 'end' : ''}`
      ].join(' ')}>
        {
          showTimestamp &&
          <div className="timestamp">
            {friendlyTimestamp}
          </div>
        }

        <div id="bubble-outer" className="bubble-container">
          {isMine &&
            <Icon id="delete-msg-btn" icon={closeSharp} onClick={this.handleDeleteClick} />
          }
          <div className="bubble" title={friendlyTimestamp}>
            {data.message}
          </div>
        </div>
      </div>
    );
  }
}

export default Message;
