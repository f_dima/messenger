import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './ConversationSearch.css';

import {
  setSearchText,
} from '../../redux/actions/conversationSearchActions';

import {
  contactListRequest,
} from '../../redux/actions/contactListActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  searchText: PropTypes.string,
  groupId: PropTypes.number,
  contactList: PropTypes.array,
}

class ConversationSearch extends Component {
  constructor(props) {
    super();

    this.handleSetSearchText = this.handleSetSearchText.bind(this);
  }

  handleSetSearchText(e) {
    if (e.target.value.length > 2) {
      this.props.dispatch(setSearchText(e.target.value));
      this.props.dispatch(contactListRequest(this.props.groupId, 1, this.props.contactList, e.target.value));
    } else {
      this.props.dispatch(setSearchText(''));
      this.props.dispatch(contactListRequest(this.props.groupId, 1, this.props.contactList, ''));
    }
  }

  render() {
    return (
      <div className="conversation-search">
        <input
          type="search"
          className="conversation-search-input"
          placeholder="Поиск"
          onChange={this.handleSetSearchText}
        />
      </div>
    );
  }
}

ConversationSearch.propTypes = propTypes;

function mapStateToProps(state) {
  const {searchText} = (state.conversationSearch);

  const {
    groupId,
    contactList,
  } = (state.conversationList);

  return {
    groupId,
    contactList,
    searchText,
  }
}

export default connect(mapStateToProps)(ConversationSearch);