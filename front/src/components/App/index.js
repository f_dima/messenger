import React, { Component } from 'react';
import Messenger from '../Messenger';
import SettingsPage from '../SettingsPage';
import { Switch, Route } from 'react-router-dom';
import WelcomePage from '../WelcomePage';
import Register from '../Register';
import ConfirmPage from '../ConfirmPage';
import SearchPage from '../SearchPage';

class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' render={matchProps => (
          <Messenger {...matchProps} />
        )} />
        <Route path='/settings' render={matchProps => (
          <SettingsPage {...matchProps} />
        )} />
        <Route path='/login' component={WelcomePage} />
        <Route path='/register' render={matchProps => (
          <Register {...matchProps} />
        )} />
        <Route path='/accounts/confirm' render={matchProps => (
          <ConfirmPage {...matchProps} />
        )} />
        <Route path='/search' render={matchProps => (
          <SearchPage {...matchProps} />
        )} />
      </Switch>
    );
  }
}

export default App;