import React, { Component, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from '../Toolbar';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import './Register.css';

import {
  registerAdd,
  registerValidated,
  registerSetEmail,
  registerSetPassword,
  registerSetFirstname,
  registerSetLastname,
  registerShowConfirm,
  registerShowDismiss,
} from '../../redux/actions/registerActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  email: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  password: PropTypes.string,
  validated: PropTypes.bool,
  token: PropTypes.string,
  showConfirm: PropTypes.bool,
  showDismiss: PropTypes.bool,
}

class Register extends Component {
  constructor(props) {
    super()

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeFirstname = this.handleChangeFirstname.bind(this);
    this.handleChangeLastname = this.handleChangeLastname.bind(this);
    this.handleCloseConfirm = this.handleCloseConfirm.bind(this);
    this.handleCloseDismiss = this.handleCloseDismiss.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    event.preventDefault();
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
      this.props.dispatch(registerValidated(true));
    } else {
      const {
        email,
        password,
        firstName,
        lastName,
      } = this.props;
      this.props.dispatch(registerAdd(email, password, firstName, lastName))
    }
  };

  handleChangeEmail(event) {
    this.props.dispatch(registerSetEmail(event.target.value))
  }

  handleChangePassword(event) {
    this.props.dispatch(registerSetPassword(event.target.value))
  }

  handleChangeFirstname(event) {
    this.props.dispatch(registerSetFirstname(event.target.value))
  }

  handleChangeLastname(event) {
    this.props.dispatch(registerSetLastname(event.target.value))
  }

  handleCloseConfirm() {
    this.props.dispatch(registerShowConfirm(false))
  }

  handleCloseDismiss() {
    this.props.dispatch(registerShowDismiss(false))
  }

  handleConfirm() {
    const {token} = this.props;
    window.location.href = '/accounts/confirm?token='+token;
  }

  render() {
    const { validated, showConfirm, showDismiss, email, token } = this.props;
    return (
      <div className="register-title">
        <Toolbar
          title="Регистрация"
        />
        <Card className="register-card" body>
          <Form noValidate validated={validated} onSubmit={this.handleSubmit} className="register-form">
            <Form.Row>
              <Form.Group className="register-email-form" controlId="formGridEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  required
                  type="email"
                  placeholder="example@mail.ru"
                  onChange={this.handleChangeEmail}
                />
                <Form.Control.Feedback type="invalid">Введите email</Form.Control.Feedback>
              </Form.Group>

              <Form.Group className="register-password-form" controlId="formGridPassword">
                <Form.Label>Пароль</Form.Label>
                <Form.Control
                  required
                  type="password"
                  placeholder="Введите пароль"
                  onChange={this.handleChangePassword}
                />
                <Form.Control.Feedback type="invalid">Введите пароль</Form.Control.Feedback>
              </Form.Group>
            </Form.Row>

            <Form.Group controlId="formGridFirstname">
              <Form.Label>Имя</Form.Label>
              <Form.Control
                required
                type="text"
                onChange={this.handleChangeFirstname}
              />
              <Form.Control.Feedback type="invalid">Введите имя</Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId="formGridLastname">
              <Form.Label>Фамилия</Form.Label>
              <Form.Control
                type="text"
                required
                onChange={this.handleChangeLastname}
              />
              <Form.Control.Feedback type="invalid">Введите фамилию</Form.Control.Feedback>
            </Form.Group>
            <div className="register-button" >
              <Button variant="primary" type="submit">
                Подтвердить
              </Button>
            </div>
          </Form>
          <Alert 
          variant="danger"
          onClose={this.handleCloseDismiss}
          dismissible
          show={showDismiss}
          >
            <Alert.Heading>Ошибка!</Alert.Heading>
            <p>
              Пользователь с таким логином уже существует.
            </p>
          </Alert>
        </Card>
        <Modal
          show={showConfirm}
          onHide={this.handleCloseConfirm}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Подтвердите адрес электронной почты
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p id="response-text">
              На адрес {email} выслано письмо с подтверждением (<a href="#" onClick={this.handleConfirm}>{'http://'+window.location.hostname+'/accounts/confirm?token='+token}</a>)
            </p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.handleCloseConfirm}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

Register.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    email,
    firstName,
    lastName,
    password,
    validated,
    token,
    showConfirm,
    showDismiss,
  } = (state.register);

  return {
    email,
    firstName,
    lastName,
    password,
    validated,
    token,
    showConfirm,
    showDismiss,
  };
}

export default connect(mapStateToProps)(Register);