import React, { Component } from 'react';
import './SettingsPage.css';
import SettingsMenu from '../SettingsMenu';
import SettingsProfile from '../SettingsProfile';
import SettingsGroups from '../SettingsGroups';
import DonatePage from '../DonatePage'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AdminPage from '../AdminPage';

import {
  getSettingsProfile,
  setLocationSettings,
  getGroupList,
} from '../../redux/actions/settingsPageActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  email: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  photo_url: PropTypes.string,
  groups: PropTypes.array,
  location: PropTypes.string,
  is_admin: PropTypes.bool,
}

class SettingsPage extends Component {
  constructor(props) {
    super();

    props.dispatch(getSettingsProfile());
    props.dispatch(getGroupList());
  }

  render() {
    const {
      email,
      firstName,
      lastName,
      photo_url,
      groups,
      location,
      is_admin,
    } = this.props;

    if (window.location.pathname.includes('groups')) {
      this.props.dispatch(setLocationSettings('groups'));
    } else if (window.location.pathname.includes('donate')){
      this.props.dispatch(setLocationSettings('donate'));
    } else if (window.location.pathname.includes('admin')) {
      this.props.dispatch(setLocationSettings('admin'));
    } else {
      this.props.dispatch(setLocationSettings('user'));
    }

    return (
      <div className="settings-page" >

        <div className="scrollable sidebar">
          <SettingsMenu
            groups={groups}
            is_admin={is_admin}
          />
        </div>
        { location === 'user' &&
          <div className="scrollable content">
            <SettingsProfile
              email={email}
              firstName={firstName}
              lastName={lastName}
              photo_url={photo_url}
            />
          </div>
        }
        { location === 'groups' &&
          <div className="scrollable content">
            <SettingsGroups
              groups={groups}
            />
          </div>
        }
        { location === 'donate' &&
          <DonatePage />
        }
        { location === 'admin' &&
          <AdminPage />
        }

      </div>
    );
  }
}

SettingsPage.propTypes = propTypes;

function mapStateToProps(state) {
  const {
    email,
    firstName,
    lastName,
    photo_url,
    groups,
    location,
    is_admin,
  } = (state.settingsPage);

  return {
    email,
    firstName,
    lastName,
    photo_url,
    groups,
    location,
    is_admin,
  }
}

export default connect(mapStateToProps)(SettingsPage);